//
//  IRCoreDataStack.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 23/11/17.
//  Copyright © 2017 Ruben Zilibowitz. All rights reserved.
//

import Foundation
import CoreData

class IRCoreDataStack: NSObject {
    
    // Can't init is singleton
    private override init() { }
    
    static let shared = IRCoreDataStack()
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Independent_Reserve")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                NSLog("Unresolved error \(error), \(error.userInfo)")
            }
        })
        container.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        return container
    }()
    
    var mainContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    /*
    lazy var backgroundContext: NSManagedObjectContext = {
        let backgroundContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        backgroundContext.parent = self.persistentContainer.viewContext
        return backgroundContext
    }()
    */
    
    /** Create an in memory persistent container. Use for unit testing. */
    func createInMemoryPersistentContainer() throws -> NSPersistentContainer {
        let container = NSPersistentContainer(name: "Independent_Reserve")
        let description = NSPersistentStoreDescription()
        description.type = NSInMemoryStoreType
        description.configuration = "Default"
        container.persistentStoreDescriptions = [description]
        
        var loadError: Error?
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            loadError = error
        })
        if let error = loadError {
            throw error
        }
        return container
    }
    
    /** Deletes and re-creates persistent stores. */
    func clearPersistentStores() {
        do {
            for store in persistentContainer.persistentStoreCoordinator.persistentStores {
                guard let url = store.url else { continue }
                
                try persistentContainer.persistentStoreCoordinator.destroyPersistentStore(at: url, ofType: store.type, options: nil)
                
                try persistentContainer.persistentStoreCoordinator.addPersistentStore(ofType: store.type, configurationName: store.configurationName, at: url, options: store.options)
            }
        }
        catch {
            NSLog(error.localizedDescription)
        }
    }
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
