//
//  Extensions.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 22/5/18.
//  Copyright © 2018 Ruben Zilibowitz. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension FxRate {
    var key: String? {
        guard let codeA = self.relatedCurrencyA?.code, let codeB = self.relatedCurrencyB?.code else { return nil }
        return "\((codeA,codeB))"
    }
}

extension Date {
    var shortString: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        return formatter.string(from: self)
    }
}

extension Bundle {
    var versionString: String {
        let bundleVersionShortString = self.infoDictionary?["CFBundleShortVersionString"] as? String
        let bundleVersion = self.infoDictionary?["CFBundleVersion"] as? String
        let release = "\(bundleVersionShortString ?? "?") (\(bundleVersion ?? "?"))"
        return release
    }
}

extension UIViewController {
    func presentError(message: String?) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .`default`, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
