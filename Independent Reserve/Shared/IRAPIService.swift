//
//  IRAPIService.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 21/11/17.
//  Copyright © 2017 Ruben Zilibowitz. All rights reserved.
//

// https://www.independentreserve.com/API

import Foundation
import CoreData

/** Transform the success type of a Result. */
infix operator >>>
func >>><T, U> (left: IRAPIService.Result<T>, right: (T) throws -> U) -> IRAPIService.Result<U> {
    switch left {
    case .success(let result):
        do {
            let transformedResult = try right(result)
            return .success(transformedResult)
        }
        catch {
            return .failure(error)
        }
    case .failure(let error):
        return .failure(error)
    }
}

/** Singleton for API calls. */
class IRAPIService {
    
    // singleton
    private init() {
    }
    
    static let shared = IRAPIService()
    
    /** Result of an API call. */
    enum Result<T> {
        
        /** API call successfull. */
        case success(T)
        
        /** There was an error. */
        case failure(Error?)
    }
    
    /** Error message class. */
    class APIError: LocalizedError {
        var message: String
        init(_ message: String) {
            self.message = message
        }
        
        var errorDescription: String? {
            return message
        }
    }
    
    let networkClient: IRNetworkClient = IRHTTPClient(session: URLSession.shared)
    let baseUrl = "https://api.independentreserve.com/"
    
    // MARK: - Public API
    
    func GetValidPrimaryCurrencyCodes(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, completion: @escaping (Result<Int>) -> Void) {
        let apiCompletion: ((Result<[String]>) -> Void) = {document in
            moc.perform {
                completion(document >>> {codesFromAPI in
                    let fetchRequest: NSFetchRequest<ValidPrimaryCurrencyCode> = NSFetchRequest<ValidPrimaryCurrencyCode>(entityName: "ValidPrimaryCurrencyCode")
                    let fetchedObjects = try fetchRequest.execute()
                    
                    // remove unused codes
                    for validCode in fetchedObjects {
                        if let codeString = validCode.code, !codesFromAPI.contains(codeString) {
                            moc.delete(validCode)
                        }
                    }
                    
                    // insert new codes
                    for validCodeFromAPI in codesFromAPI {
                        if !fetchedObjects.compactMap({$0.code}).contains(validCodeFromAPI) {
                            guard let validCode = NSEntityDescription.insertNewObject(forEntityName: "ValidPrimaryCurrencyCode", into: moc) as? ValidPrimaryCurrencyCode else { throw APIError("insertNewObject ValidPrimaryCurrencyCode failed") }
                            validCode.code = validCodeFromAPI
                            validCode.isPrimary = true
                        }
                    }
                    
                    // save changes
                    if moc.hasChanges {
                        try moc.save()
                    }
                    
                    return codesFromAPI.count}
                )
            }
        }
        getWith(path: "Public/GetValidPrimaryCurrencyCodes", completion: apiCompletion)
    }
    
    func GetValidSecondaryCurrencyCodes(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, completion: @escaping (Result<Int>) -> Void) {
        let apiCompletion: ((Result<[String]>) -> Void) = {document in
            moc.perform {
                completion(document >>> {codesFromAPI in
                    let fetchRequest: NSFetchRequest<ValidSecondaryCurrencyCode> = NSFetchRequest<ValidSecondaryCurrencyCode>(entityName: "ValidSecondaryCurrencyCode")
                    let fetchedObjects = try fetchRequest.execute()
                    
                    // remove unused codes
                    for validCode in fetchedObjects {
                        if let codeString = validCode.code, !codesFromAPI.contains(codeString) {
                            moc.delete(validCode)
                        }
                    }
                    
                    // insert new codes
                    for validCodeFromAPI in codesFromAPI {
                        if !fetchedObjects.compactMap({$0.code}).contains(validCodeFromAPI) {
                            guard let validCode = NSEntityDescription.insertNewObject(forEntityName: "ValidSecondaryCurrencyCode", into: moc) as? ValidSecondaryCurrencyCode else { throw APIError("insertNewObject ValidSecondaryCurrencyCode failed") }
                            validCode.code = validCodeFromAPI
                            validCode.isPrimary = false
                        }
                    }
                    
                    // save changes
                    if moc.hasChanges {
                        try moc.save()
                    }
                    
                    return codesFromAPI.count}
                )
            }
        }
        getWith(path: "Public/GetValidSecondaryCurrencyCodes", completion: apiCompletion)
    }
    
    func GetValidLimitOrderTypes(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, completion: @escaping (Result<Int>) -> Void) {
        let apiCompletion: ((Result<[String]>) -> Void) = {document in
            moc.perform {
                completion(document >>> {limitOrderTypesFromAPI in
                    let fetchRequest: NSFetchRequest<ValidOrderType> = NSFetchRequest<ValidOrderType>(entityName: "ValidOrderType")
                    fetchRequest.predicate = NSPredicate(format: "isLimitOrder = TRUE")
                    let fetchedObjects = try fetchRequest.execute()
                    
                    // remove unused codes
                    for validLimitOrder in fetchedObjects {
                        if let typeString = validLimitOrder.type, !limitOrderTypesFromAPI.contains(typeString) {
                            moc.delete(validLimitOrder)
                        }
                    }
                    
                    // insert new codes
                    for typeFromAPI in limitOrderTypesFromAPI {
                        if !fetchedObjects.compactMap({$0.type}).contains(typeFromAPI) {
                            guard let validType = NSEntityDescription.insertNewObject(forEntityName: "ValidOrderType", into: moc) as? ValidOrderType else { throw APIError("insertNewObject ValidOrderType failed") }
                            validType.type = typeFromAPI
                            validType.isLimitOrder = true
                            validType.isBid = typeFromAPI.contains("Bid")
                        }
                    }
                    
                    // save changes
                    if moc.hasChanges {
                        try moc.save()
                    }
                    
                    return limitOrderTypesFromAPI.count}
                )
            }
        }
        getWith(path: "Public/GetValidLimitOrderTypes", completion: apiCompletion)
    }
    
    func GetValidMarketOrderTypes(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, completion: @escaping (Result<Int>) -> Void) {
        let apiCompletion: ((Result<[String]>) -> Void) = {document in
            moc.perform {
                completion(document >>> {marketOrderTypesFromAPI in
                    let fetchRequest: NSFetchRequest<ValidOrderType> = NSFetchRequest<ValidOrderType>(entityName: "ValidOrderType")
                    fetchRequest.predicate = NSPredicate(format: "isLimitOrder = FALSE")
                    let fetchedObjects = try fetchRequest.execute()
                    
                    // remove unused codes
                    for validMarketOrder in fetchedObjects {
                        if let typeString = validMarketOrder.type, !marketOrderTypesFromAPI.contains(typeString) {
                            moc.delete(validMarketOrder)
                        }
                    }
                    
                    // insert new codes
                    for typeFromAPI in marketOrderTypesFromAPI {
                        if !fetchedObjects.compactMap({$0.type}).contains(typeFromAPI) {
                            guard let validType = NSEntityDescription.insertNewObject(forEntityName: "ValidOrderType", into: moc) as? ValidOrderType else { throw APIError("insertNewObject ValidOrderType failed") }
                            validType.type = typeFromAPI
                            validType.isLimitOrder = false
                            validType.isBid = typeFromAPI.contains("Bid")
                        }
                    }
                    
                    // save changes
                    if moc.hasChanges {
                        try moc.save()
                    }
                    
                    return marketOrderTypesFromAPI.count}
                )
            }
        }
        getWith(path: "Public/GetValidMarketOrderTypes", completion: apiCompletion)
    }
    
    func GetValidOrderTypes(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, completion: @escaping (Result<Int>) -> Void) {
        let apiCompletion: ((Result<[String]>) -> Void) = {document in
            moc.perform {
                completion(document >>> {orderTypesFromAPI in
                    let fetchRequest: NSFetchRequest<ValidOrderType> = NSFetchRequest<ValidOrderType>(entityName: "ValidOrderType")
                    let fetchedObjects = try fetchRequest.execute()
                    
                    // remove unused codes
                    for validOrder in fetchedObjects {
                        if let typeString = validOrder.type, !orderTypesFromAPI.contains(typeString) {
                            moc.delete(validOrder)
                        }
                    }
                    
                    // insert new codes
                    for typeFromAPI in orderTypesFromAPI {
                        if !fetchedObjects.compactMap({$0.type}).contains(typeFromAPI) {
                            guard let validType = NSEntityDescription.insertNewObject(forEntityName: "ValidOrderType", into: moc) as? ValidOrderType else { throw APIError("insertNewObject ValidOrderType failed") }
                            validType.type = typeFromAPI
                            validType.isLimitOrder = typeFromAPI.contains("Limit")   // check if type contains word "limit"
                            validType.isBid = typeFromAPI.contains("Bid")
                        }
                    }
                    
                    // save changes
                    if moc.hasChanges {
                        try moc.save()
                    }
                    
                    return orderTypesFromAPI.count}
                )
            }
        }
        getWith(path: "Public/GetValidOrderTypes", completion: apiCompletion)
    }
    
    func GetValidTransactionTypes(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, completion: @escaping (Result<Int>) -> Void) {
        let apiCompletion: ((Result<[String]>) -> Void) = {document in
            moc.perform {
                completion(document >>> {transactionTypesFromAPI in
                    let fetchRequest: NSFetchRequest<ValidTransactionType> = NSFetchRequest<ValidTransactionType>(entityName: "ValidTransactionType")
                    let fetchedObjects = try fetchRequest.execute()
                    
                    // remove unused codes
                    for validTransaction in fetchedObjects {
                        if let typeString = validTransaction.type, !transactionTypesFromAPI.contains(typeString) {
                            moc.delete(validTransaction)
                        }
                    }
                    
                    // insert new codes
                    for typeFromAPI in transactionTypesFromAPI {
                        if !fetchedObjects.compactMap({$0.type}).contains(typeFromAPI) {
                            guard let validType = NSEntityDescription.insertNewObject(forEntityName: "ValidTransactionType", into: moc) as? ValidTransactionType else { throw APIError("insertNewObject ValidTransactionType failed") }
                            validType.type = typeFromAPI
                        }
                    }
                    
                    // save changes
                    if moc.hasChanges {
                        try moc.save()
                    }
                    
                    return transactionTypesFromAPI.count}
                )
            }
        }
        getWith(path: "Public/GetValidTransactionTypes", completion: apiCompletion)
    }
    
    func fetchFirstObject<T:NSFetchRequestResult>(satisfying predicate: NSPredicate? = nil) throws -> T? {
        let className = String(describing: T.self)
        let fetchRequest: NSFetchRequest<T> = NSFetchRequest<T>(entityName: className)
        if let predicate = predicate {
            fetchRequest.predicate = predicate
        }
        let fetchedObjects = try fetchRequest.execute()
        return fetchedObjects.first
    }
    
    func fetchObjects<T:NSFetchRequestResult>(satisfying predicate: NSPredicate? = nil) throws -> [T] {
        let className = String(describing: T.self)
        let fetchRequest: NSFetchRequest<T> = NSFetchRequest<T>(entityName: className)
        if let predicate = predicate {
            fetchRequest.predicate = predicate
        }
        let fetchedObjects = try fetchRequest.execute()
        return fetchedObjects
    }
    
    func fetchObjectsAsDictionary<K:Hashable,T:NSFetchRequestResult>(keyFunction: (T) -> K?, predicate: NSPredicate? = nil) throws -> [K:T] {
        let className = String(describing: T.self)
        let fetchRequest: NSFetchRequest<T> = NSFetchRequest<T>(entityName: className)
        if let predicate = predicate {
            fetchRequest.predicate = predicate
        }
        let fetchedObjects = try fetchRequest.execute()
        
        var fetchedObjectsByID : [K : T] = [:]
        for object in fetchedObjects {
            if let key = keyFunction(object) {
                fetchedObjectsByID[key] = object
            }
        }
        
        return fetchedObjectsByID
    }
    
    func GetMarketSummary(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, primaryCurrencyCode:String, secondaryCurrencyCode:String, completion: @escaping (Result<MarketSummary>) -> Void) {
        let apiCompletion: ((Result<[String:AnyObject]>) -> Void) = {document in
            moc.perform {
                completion(document >>> {[weak self] marketSummaryFromAPI in
                    guard let weakSelf = self else { throw APIError("no self") }
                    
                    guard let marketSummary = NSEntityDescription.insertNewObject(forEntityName: "MarketSummary", into: moc) as? MarketSummary else { throw APIError("insertNewObject MarketSummary failed") }
                    
                    guard let createdTimestamp = (marketSummaryFromAPI["CreatedTimestampUtc"] as? String)?.dateUTC else { throw APIError("CreatedTimestampUtc failed to parse") }
                    guard let currentHighestBidPrice = marketSummaryFromAPI["CurrentHighestBidPrice"] as? Double else { throw APIError("CurrentHighestBidPrice failed to parse") }
                    guard let currentLowestOfferPrice = marketSummaryFromAPI["CurrentLowestOfferPrice"] as? Double else { throw APIError("CurrentLowestOfferPrice failed to parse") }
                    guard let dayAvgPrice = marketSummaryFromAPI["DayAvgPrice"] as? Double else { throw APIError("DayAvgPrice failed to parse") }
                    guard let dayHighestPrice = marketSummaryFromAPI["DayHighestPrice"] as? Double else { throw APIError("DayHighestPrice failed to parse") }
                    guard let dayLowestPrice = marketSummaryFromAPI["DayLowestPrice"] as? Double else { throw APIError("DayLowestPrice failed to parse") }
                    guard let dayVolumeXbt = marketSummaryFromAPI["DayVolumeXbt"] as? Double else { throw APIError("DayVolumeXbt failed to parse") }
                    let dayVolumeXbtInSecondaryCurrrency = marketSummaryFromAPI["DayVolumeXbtInSecondaryCurrrency"] as? Double
                    guard let lastPrice = marketSummaryFromAPI["LastPrice"] as? Double else { throw APIError("LastPrice failed to parse") }
                    guard let primaryCurrencyCode = marketSummaryFromAPI["PrimaryCurrencyCode"] as? String else { throw APIError("PrimaryCurrencyCode failed to parse") }
                    guard let secondaryCurrencyCode = marketSummaryFromAPI["SecondaryCurrencyCode"] as? String else { throw APIError("SecondaryCurrencyCode failed to parse") }
                    
                    guard let primaryCurrencyCodeObject:ValidPrimaryCurrencyCode = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "code = %@", primaryCurrencyCode)) else { throw APIError("Could not find primary currency code \(primaryCurrencyCode)") }
                    guard let secondaryCurrencyCodeObject:ValidSecondaryCurrencyCode = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "code = %@", secondaryCurrencyCode)) else { throw APIError("Could not find secondary currency code \(secondaryCurrencyCode)") }
                    
                    marketSummary.createdTimestamp = createdTimestamp
                    marketSummary.currentHighestBidPrice = currentHighestBidPrice
                    marketSummary.currentLowestOfferPrice = currentLowestOfferPrice
                    marketSummary.dayAveragePrice = dayAvgPrice
                    marketSummary.dayHighestPrice = dayHighestPrice
                    marketSummary.dayLowestPrice = dayLowestPrice
                    marketSummary.dayVolumeXbt = dayVolumeXbt
                    marketSummary.dayVolumeXbtInSecondaryCurrency = dayVolumeXbtInSecondaryCurrrency ?? 0
                    marketSummary.lastPrice = lastPrice
                    marketSummary.relatedPrimaryCurrency = primaryCurrencyCodeObject
                    marketSummary.relatedSecondaryCurrency = secondaryCurrencyCodeObject
                    
                    // save changes
                    if moc.hasChanges {
                        try moc.save()
                    }
                    
                    return marketSummary
                    }
                )
            }
        }
        getWith(path: "Public/GetMarketSummary", parameters: ["primaryCurrencyCode":primaryCurrencyCode,
                                                              "secondaryCurrencyCode":secondaryCurrencyCode],
                completion: apiCompletion)
    }
    
    func GetOrderBook(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, primaryCurrencyCode:String, secondaryCurrencyCode:String, completion: @escaping (Result<OrderBook>) -> Void) {
        let apiCompletion: ((Result<[String:AnyObject]>) -> Void) = {document in
            moc.perform {
                completion(document >>> {[weak self] orderBookFromAPI in
                    guard let weakSelf = self else { throw APIError("no self") }
                    
                    guard let orderBook = NSEntityDescription.insertNewObject(forEntityName: "OrderBook", into: moc) as? OrderBook else { throw APIError("insertNewObject OrderBook failed") }
                    
                    guard let createdTimestamp = (orderBookFromAPI["CreatedTimestampUtc"] as? String)?.dateUTC else { throw APIError("CreatedTimestampUtc failed to parse") }
                    
                    guard let primaryCurrencyCode = orderBookFromAPI["PrimaryCurrencyCode"] as? String else { throw APIError("PrimaryCurrencyCode failed to parse") }
                    guard let secondaryCurrencyCode = orderBookFromAPI["SecondaryCurrencyCode"] as? String else { throw APIError("SecondaryCurrencyCode failed to parse") }
                    
                    guard let primaryCurrencyCodeObject:ValidPrimaryCurrencyCode = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "code = %@", primaryCurrencyCode)) else { throw APIError("Could not find primary currency code \(primaryCurrencyCode)") }
                    guard let secondaryCurrencyCodeObject:ValidSecondaryCurrencyCode = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "code = %@", secondaryCurrencyCode)) else { throw APIError("Could not find secondary currency code \(secondaryCurrencyCode)") }
                    
                    guard let buyOrders = (orderBookFromAPI["BuyOrders"] as? [[String:AnyObject]]) else { throw APIError("BuyOrders failed to parse") }
                    guard let sellOrders = (orderBookFromAPI["SellOrders"] as? [[String:AnyObject]]) else { throw APIError("BuyOrders failed to parse") }
                    
                    orderBook.createdTimestamp = createdTimestamp
                    orderBook.relatedBuyOrders = nil
                    orderBook.relatedSellOrders = nil
                    orderBook.relatedPrimaryCurrency = primaryCurrencyCodeObject
                    orderBook.relatedSecondaryCurrency = secondaryCurrencyCodeObject
                    
                    // buy orders
                    for order in buyOrders {
                        guard let orderType = order["OrderType"] as? String else { continue }
                        guard let price = order["Price"] as? Double else { continue }
                        guard let volume = order["Volume"] as? Double else { continue }
                        guard let orderTypeObject:ValidOrderType = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "type = %@", orderType)) else { throw APIError("Could not find primary currency code \(primaryCurrencyCode)") }
                        guard let order = NSEntityDescription.insertNewObject(forEntityName: "PublicOrder", into: moc) as? PublicOrder else { throw APIError("insertNewObject PublicOrder failed") }
                        order.price = price
                        order.volume = volume
                        order.relatedOrderType = orderTypeObject
                        order.relatedOrderBookBuy = orderBook
                        order.relatedOrderBookSell = nil
                    }
                    
                    // sell orders
                    for order in sellOrders {
                        guard let orderType = order["OrderType"] as? String else { continue }
                        guard let price = order["Price"] as? Double else { continue }
                        guard let volume = order["Volume"] as? Double else { continue }
                        guard let orderTypeObject:ValidOrderType = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "type = %@", orderType)) else { throw APIError("Could not find primary currency code \(primaryCurrencyCode)") }
                        guard let order = NSEntityDescription.insertNewObject(forEntityName: "PublicOrder", into: moc) as? PublicOrder else { throw APIError("insertNewObject PublicOrder failed") }
                        order.price = price
                        order.volume = volume
                        order.relatedOrderType = orderTypeObject
                        order.relatedOrderBookBuy = nil
                        order.relatedOrderBookSell = orderBook
                    }

                    // save changes
                    if moc.hasChanges {
                        try moc.save()
                    }
                    
                    return orderBook
                    }
                )
            }
        }
        getWith(path: "Public/GetOrderBook", parameters: ["primaryCurrencyCode":primaryCurrencyCode,
                                                          "secondaryCurrencyCode":secondaryCurrencyCode],
                completion: apiCompletion)
    }
    
    func GetTradeHistorySummary(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, primaryCurrencyCode:String, secondaryCurrencyCode:String, numberOfHoursInThePastToRetrieve:Int = 24, completion: @escaping (Result<Int>) -> Void) {
        let apiCompletion: ((Result<[String:AnyObject]>) -> Void) = {document in
            moc.perform {
                completion(document >>> {[weak self] tradeHistoryFromAPI in
                    guard let weakSelf = self else { throw APIError("no self") }
                    
                    guard let _ = (tradeHistoryFromAPI["CreatedTimestampUtc"] as? String)?.dateUTC else { throw APIError("CreatedTimestampUtc failed to parse") }
                    guard let items = tradeHistoryFromAPI["HistorySummaryItems"] as? [[String:AnyObject]] else { throw APIError("HistorySummaryItems failed to parse") }
                    
                    // delete old items
                    let oldSummaryItems: [TradeHistorySummaryItem] = try weakSelf.fetchObjects()
                    oldSummaryItems.forEach({moc.delete($0)})
                    
                    guard let primaryCurrencyCodeObject:ValidPrimaryCurrencyCode = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "code = %@", primaryCurrencyCode)) ?? NSEntityDescription.insertNewObject(forEntityName: "ValidPrimaryCurrencyCode", into: moc) as? ValidPrimaryCurrencyCode else { throw APIError("Could not find currency code \(primaryCurrencyCode)") }
                    primaryCurrencyCodeObject.code = primaryCurrencyCode
                    
                    guard let secondaryCurrencyCodeObject:ValidSecondaryCurrencyCode = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "code = %@", secondaryCurrencyCode)) ?? NSEntityDescription.insertNewObject(forEntityName: "ValidSecondaryCurrencyCode", into: moc) as? ValidSecondaryCurrencyCode else { throw APIError("Could not find currency code \(secondaryCurrencyCode)") }
                    secondaryCurrencyCodeObject.code = secondaryCurrencyCode
                    
                    for item in items {
                        guard let averageSecondaryCurrencyPrice = item["AverageSecondaryCurrencyPrice"] as? Double else { continue }
                        guard let closingSecondaryCurrencyPrice = item["ClosingSecondaryCurrencyPrice"] as? Double else { continue }
                        guard let startTimestamp = (item["StartTimestampUtc"] as? String)?.dateShortUTC else { continue }
                        guard let endTimestamp = (item["EndTimestampUtc"] as? String)?.dateShortUTC else { continue }
                        guard let highestSecondaryCurrencyPrice = item["HighestSecondaryCurrencyPrice"] as? Double else { continue }
                        guard let lowestSecondaryCurrencyPrice = item["LowestSecondaryCurrencyPrice"] as? Double else { continue }
                        guard let numberOfTrades = item["NumberOfTrades"] as? Int32 else { continue }
                        guard let openingSecondaryCurrencyPrice = item["OpeningSecondaryCurrencyPrice"] as? Double else { continue }
                        guard let primaryCurrencyVolume = item["PrimaryCurrencyVolume"] as? Double else { continue }
                        guard let secondaryCurrencyVolume = item["SecondaryCurrencyVolume"] as? Double else { continue }
                        
                        guard let summaryItem = NSEntityDescription.insertNewObject(forEntityName: "TradeHistorySummaryItem", into: moc) as? TradeHistorySummaryItem else { continue }
                        
                        summaryItem.averageSecondaryCurrencyPrice = averageSecondaryCurrencyPrice
                        summaryItem.closingSecondaryCurrencyPrice = closingSecondaryCurrencyPrice
                        summaryItem.startTimestamp = startTimestamp
                        summaryItem.endTimestamp = endTimestamp
                        summaryItem.highestSecondaryCurrencyPrice = highestSecondaryCurrencyPrice
                        summaryItem.lowestSecondaryCurrencyPrice = lowestSecondaryCurrencyPrice
                        summaryItem.numberOfTrades = numberOfTrades
                        summaryItem.openingSecondaryCurrencyPrice = openingSecondaryCurrencyPrice
                        summaryItem.primaryCurrencyVolume = primaryCurrencyVolume
                        summaryItem.secondaryCurrencyVolume = secondaryCurrencyVolume
                        
                        summaryItem.relatedPrimaryCurrency = primaryCurrencyCodeObject
                        summaryItem.relatedSecondaryCurrency = secondaryCurrencyCodeObject
                    }
                    
                    // save changes
                    if moc.hasChanges {
                        try moc.save()
                    }

                    return items.count
                    }
                )
            }
        }
        getWith(path: "Public/GetTradeHistorySummary", parameters: ["primaryCurrencyCode":primaryCurrencyCode,
                                                                    "secondaryCurrencyCode":secondaryCurrencyCode,
                                                                    "numberOfHoursInThePastToRetrieve":"\(numberOfHoursInThePastToRetrieve)"],
                completion: apiCompletion)
    }
    
    func GetRecentTrades(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, primaryCurrencyCode:String, secondaryCurrencyCode:String, numberOfRecentTradesToRetrieve:Int = 10, completion: @escaping (Result<RecentTrades>) -> Void) {
        let apiCompletion: ((Result<[String:AnyObject]>) -> Void) = {document in
            moc.perform {
                completion(document >>> {[weak self] recentTradesFromAPI in
                    guard let weakSelf = self else { throw APIError("no self") }
                    
                    guard let createdTimestamp = (recentTradesFromAPI["CreatedTimestampUtc"] as? String)?.dateUTC else { throw APIError("CreatedTimestampUtc failed to parse") }
                    
                    guard let primaryCurrencyCodeObject:ValidPrimaryCurrencyCode = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "code = %@", primaryCurrencyCode)) ?? NSEntityDescription.insertNewObject(forEntityName: "ValidPrimaryCurrencyCode", into: moc) as? ValidPrimaryCurrencyCode else { throw APIError("Could not find currency code \(primaryCurrencyCode)") }
                    primaryCurrencyCodeObject.code = primaryCurrencyCode
                    
                    guard let secondaryCurrencyCodeObject:ValidSecondaryCurrencyCode = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "code = %@", secondaryCurrencyCode)) ?? NSEntityDescription.insertNewObject(forEntityName: "ValidSecondaryCurrencyCode", into: moc) as? ValidSecondaryCurrencyCode else { throw APIError("Could not find currency code \(secondaryCurrencyCode)") }
                    secondaryCurrencyCodeObject.code = secondaryCurrencyCode
                    
                    guard let recentTrades = NSEntityDescription.insertNewObject(forEntityName: "RecentTrades", into: moc) as? RecentTrades else { throw APIError("insertNewObject RecentTrades failed") }
                    
                    recentTrades.createdTimestamp = createdTimestamp
                    recentTrades.relatedPrimaryCurrency = primaryCurrencyCodeObject
                    recentTrades.relatedSecondaryCurrency = secondaryCurrencyCodeObject

                    guard let trades = recentTradesFromAPI["Trades"] as? [[String:AnyObject]] else { throw APIError("Trades failed to parse") }
                    
                    for tradeObject in trades {
                        guard let primaryCurrencyAmount = tradeObject["PrimaryCurrencyAmount"] as? Double else { continue }
                        guard let secondaryCurrencyTradePrice = tradeObject["SecondaryCurrencyTradePrice"] as? Double else { continue }
                        guard let tradeTimestamp = (tradeObject["TradeTimestampUtc"] as? String)?.dateUTC else { continue }
                        guard let trade = NSEntityDescription.insertNewObject(forEntityName: "PublicTrade", into: moc) as? PublicTrade else { continue }
                        trade.primaryCurrencyAmount = primaryCurrencyAmount
                        trade.secondaryCurrencyTradePrice = secondaryCurrencyTradePrice
                        trade.tradeTimestamp = tradeTimestamp
                        trade.relatedRecentTrades = recentTrades
                    }
                    
                    // save changes
                    if moc.hasChanges {
                        try moc.save()
                    }

                    return recentTrades
                    }
                )
            }
        }
        getWith(path: "Public/GetRecentTrades", parameters: ["primaryCurrencyCode":primaryCurrencyCode,
                                                             "secondaryCurrencyCode":secondaryCurrencyCode,
                                                             "numberOfRecentTradesToRetrieve":"\(numberOfRecentTradesToRetrieve)"],
                completion: apiCompletion)
    }
    
    func GetFxRates(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, completion: @escaping (Result<Int>) -> Void) {
        let apiCompletion: ((Result<[[String:AnyObject]]>) -> Void) = {document in
            moc.perform {
                completion(document >>> {[weak self] fxRatesFromAPI in
                    guard let weakSelf = self else { throw APIError("no self") }
                    
                    var fxRates: [String:FxRate] = try weakSelf.fetchObjectsAsDictionary(keyFunction: {$0.key})
                    
                    for fxRateObject in fxRatesFromAPI {
                        guard let currencyCodeA = fxRateObject["CurrencyCodeA"] as? String else { throw APIError("CurrencyCodeA failed to parse") }
                        guard let currencyCodeB = fxRateObject["CurrencyCodeB"] as? String else { throw APIError("CurrencyCodeB failed to parse") }
                        guard let rate = fxRateObject["Rate"] as? Double else { throw APIError("Rate failed to parse") }
                        
                        let key = "\((currencyCodeA,currencyCodeB))"
                        guard let fxRate = fxRates[key] ?? NSEntityDescription.insertNewObject(forEntityName: "FxRate", into: moc) as? FxRate else { throw APIError("insertNewObject FxRate failed") }
                        
                        fxRate.rate = rate
                        
                        guard let currencyCodeAObject:ValidSecondaryCurrencyCode = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "code = %@", currencyCodeA)) ?? NSEntityDescription.insertNewObject(forEntityName: "ValidSecondaryCurrencyCode", into: moc) as? ValidSecondaryCurrencyCode else { throw APIError("Could not find currency code \(currencyCodeA)") }
                        currencyCodeAObject.code = currencyCodeA
                        
                        guard let currencyCodeBObject:ValidSecondaryCurrencyCode = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "code = %@", currencyCodeB)) ?? NSEntityDescription.insertNewObject(forEntityName: "ValidSecondaryCurrencyCode", into: moc) as? ValidSecondaryCurrencyCode else { throw APIError("Could not find currency code \(currencyCodeB)") }
                        currencyCodeBObject.code = currencyCodeB
                        
                        fxRate.relatedCurrencyA = currencyCodeAObject
                        fxRate.relatedCurrencyB = currencyCodeBObject
                        
                        if let key = fxRate.key {
                            fxRates[key] = nil
                        }
                    }
                    
                    // delete unused
                    fxRates.values.forEach({moc.delete($0)})
                    
                    // save changes
                    if moc.hasChanges {
                        try moc.save()
                    }

                    return fxRatesFromAPI.count
                    }
                )
            }
        }
        getWith(path: "Public/GetFxRates", completion: apiCompletion)
    }
    
    // MARK: - Private API
    
    lazy var apiKey = {
        return UserDefaults.standard.string(forKey: IRAPIKey) ?? ""
    }()
    lazy var apiSecret = {
        return UserDefaults.standard.string(forKey: IRAPISecret) ?? ""
    }()
    var nonce:Int { return Int(Date().timeIntervalSince1970) }
    
    func PlaceLimitOrder(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, primaryCurrencyCode:String, secondaryCurrencyCode:String, orderType:String, price:Double, volume:Double, completion: @escaping (Result<String>) -> Void) {
        let parameters = [("primaryCurrencyCode",primaryCurrencyCode),
                          ("secondaryCurrencyCode",secondaryCurrencyCode),
                          ("orderType",orderType),
                          ("price","\(price)"),
                          ("volume","\(volume)")]
        privateAPI(path: "Private/PlaceLimitOrder", parameters: parameters, completion: orderDetailsAPICompletion(moc: moc, completion: completion))
    }
    
    func PlaceMarketOrder(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, primaryCurrencyCode:String, secondaryCurrencyCode:String, orderType:String, volume:Double, completion: @escaping (Result<String>) -> Void) {
        let parameters = [("primaryCurrencyCode",primaryCurrencyCode),
                          ("secondaryCurrencyCode",secondaryCurrencyCode),
                          ("orderType",orderType),
                          ("volume","\(volume)")]
        privateAPI(path: "Private/PlaceMarketOrder", parameters: parameters, completion: orderDetailsAPICompletion(moc: moc, completion: completion))
    }
    
    func CancelOrder(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, orderGuid:String, completion: @escaping (Result<String>) -> Void) {
        let parameters = [("orderGuid",orderGuid)]
        privateAPI(path: "Private/CancelOrder", parameters: parameters, completion: orderDetailsAPICompletion(moc: moc, completion: completion))
    }
    
    func orderAPICompletion(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, orderStatus: String?, primaryCurrencyCode:String? = nil, secondaryCurrencyCode:String? = nil, completion: @escaping (Result<Int>) -> Void) -> ((Result<[String:AnyObject]>) -> Void) {
        let apiCompletion: ((Result<[String:AnyObject]>) -> Void) = {document in
            moc.perform {
                completion(document >>> {[weak self] ordersFromAPI in
                    guard let weakSelf = self else { throw APIError("no self") }
                    
                    guard let orders = ordersFromAPI["Data"] as? [[String:AnyObject]] else { throw APIError("Data failed to parse") }
                    
                    var predicate = NSPredicate(format: "TRUEPREDICATE")
                    if let orderStatus = orderStatus {
                        predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate, NSPredicate(format: "status = %@", orderStatus)])
                    }
                    if let primaryCurrencyCode = primaryCurrencyCode {
                        predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate, NSPredicate(format: "relatedPrimaryCurrency.code = %@", primaryCurrencyCode)])
                    }
                    if let secondaryCurrencyCode = secondaryCurrencyCode {
                        predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate, NSPredicate(format: "relatedSecondaryCurrency.code = %@", secondaryCurrencyCode)])
                    }
                    
                    var ordersDictionary: [String:PrivateOrder] = try weakSelf.fetchObjectsAsDictionary(keyFunction: {$0.orderGuid}, predicate: predicate)
                    
                    for orderObject in orders {
                        guard let orderGuid = (orderObject["OrderGuid"] as? String) else { throw APIError("OrderGuid failed to parse") }
                        
                        guard let order = ordersDictionary[orderGuid] ?? NSEntityDescription.insertNewObject(forEntityName: "PrivateOrder", into: moc) as? PrivateOrder else { throw APIError("insertNewObject PrivateOrder failed") }
                        
                        order.orderGuid = orderGuid
                        
                        let averagePrice = orderObject["AvgPrice"] as? Double
                        guard let createdTimestampUtc = (orderObject["CreatedTimestampUtc"] as? String)?.dateUTC else { throw APIError("CreatedTimestampUtc failed to parse") }
                        guard let feePercent = orderObject["FeePercent"] as? Double else { throw APIError("FeePercent failed to parse") }
                        guard let orderType = orderObject["OrderType"] as? String else { throw APIError("OrderType failed to parse") }
                        guard let outstanding = orderObject["Outstanding"] as? Double else { throw APIError("Outstanding failed to parse") }
                        let price = orderObject["Price"] as? Double
                        guard let primaryCurrencyCode = orderObject["PrimaryCurrencyCode"] as? String else { throw APIError("PrimaryCurrencyCode failed to parse") }
                        guard let secondaryCurrencyCode = orderObject["SecondaryCurrencyCode"] as? String else { throw APIError("SecondaryCurrencyCode failed to parse") }
                        let value = orderObject["Value"] as? Double
                        guard let volume = orderObject["Volume"] as? Double else { throw APIError("Volume failed to parse") }
                        guard let status = orderObject["Status"] as? String else { throw APIError("Status failed to parse") }

                        order.averagePrice = averagePrice ?? 0
                        order.createdTimestamp = createdTimestampUtc
                        order.feePercent = feePercent
                        order.outstanding = outstanding
                        order.price = price ?? 0
                        order.value = value ?? 0
                        order.volumeOrdered = volume
                        order.volumeFilled = status.contains("Filled") ? volume : 0
                        order.reservedAmount = 0
                        order.status = status
                        
                        guard let orderTypeObject: ValidOrderType = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "type = %@", orderType)) ?? NSEntityDescription.insertNewObject(forEntityName: "ValidOrderType", into: moc) as? ValidOrderType else { throw APIError("insertNewObject ValidOrderType failed") }
                        orderTypeObject.type = orderType
                        
                        guard let primaryCurrencyCodeObject:ValidPrimaryCurrencyCode = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "code = %@", primaryCurrencyCode)) ?? NSEntityDescription.insertNewObject(forEntityName: "ValidPrimaryCurrencyCode", into: moc) as? ValidPrimaryCurrencyCode else { throw APIError("Could not find currency code \(primaryCurrencyCode)") }
                        primaryCurrencyCodeObject.code = primaryCurrencyCode
                        
                        guard let secondaryCurrencyCodeObject:ValidSecondaryCurrencyCode = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "code = %@", secondaryCurrencyCode)) ?? NSEntityDescription.insertNewObject(forEntityName: "ValidSecondaryCurrencyCode", into: moc) as? ValidSecondaryCurrencyCode else { throw APIError("Could not find currency code \(secondaryCurrencyCode)") }
                        secondaryCurrencyCodeObject.code = secondaryCurrencyCode
                        
                        order.relatedOrderType = orderTypeObject
                        order.relatedPrimaryCurrency = primaryCurrencyCodeObject
                        order.relatedSecondaryCurrency = secondaryCurrencyCodeObject
                        
                        ordersDictionary[orderGuid] = nil
                    }
                    
                    // delete unused
                    ordersDictionary.values.forEach({moc.delete($0)})
                    
                    // save changes
                    if moc.hasChanges {
                        try moc.save()
                    }

                    return orders.count
                    }
                )
            }
        }
        
        return apiCompletion
    }
    
    func GetOpenOrders(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, primaryCurrencyCode:String? = nil, secondaryCurrencyCode:String? = nil, pageIndex:Int = 1, pageSize:Int = 25, completion: @escaping (Result<Int>) -> Void) {
        var parameters:[(String,String)] = []
        if let primaryCurrencyCode = primaryCurrencyCode {
            parameters.append(("primaryCurrencyCode",primaryCurrencyCode))
        }
        if let secondaryCurrencyCode = secondaryCurrencyCode {
            parameters.append(("secondaryCurrencyCode",secondaryCurrencyCode))
        }
        parameters.append(("pageIndex","\(pageIndex)"))
        parameters.append(("pageSize","\(pageSize)"))
        privateAPI(path: "Private/GetOpenOrders", parameters: parameters, completion: orderAPICompletion(orderStatus: "Open", primaryCurrencyCode: primaryCurrencyCode, secondaryCurrencyCode: secondaryCurrencyCode, completion: completion))
    }
    
    func GetClosedOrders(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, primaryCurrencyCode:String? = nil, secondaryCurrencyCode:String? = nil, pageIndex:Int = 1, pageSize:Int = 25, completion: @escaping (Result<Int>) -> Void) {
        var parameters:[(String,String)] = []
        if let primaryCurrencyCode = primaryCurrencyCode {
            parameters.append(("primaryCurrencyCode",primaryCurrencyCode))
        }
        if let secondaryCurrencyCode = secondaryCurrencyCode {
            parameters.append(("secondaryCurrencyCode",secondaryCurrencyCode))
        }
        parameters.append(("pageIndex","\(pageIndex)"))
        parameters.append(("pageSize","\(pageSize)"))
        privateAPI(path: "Private/GetClosedOrders", parameters: parameters, completion: orderAPICompletion(orderStatus: nil, primaryCurrencyCode: primaryCurrencyCode, secondaryCurrencyCode: secondaryCurrencyCode, completion: completion))
    }
    
    func GetClosedFilledOrders(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, primaryCurrencyCode:String? = nil, secondaryCurrencyCode:String? = nil, pageIndex:Int = 1, pageSize:Int = 25, completion: @escaping (Result<Int>) -> Void) {
        var parameters:[(String,String)] = []
        if let primaryCurrencyCode = primaryCurrencyCode {
            parameters.append(("primaryCurrencyCode",primaryCurrencyCode))
        }
        if let secondaryCurrencyCode = secondaryCurrencyCode {
            parameters.append(("secondaryCurrencyCode",secondaryCurrencyCode))
        }
        parameters.append(("pageIndex","\(pageIndex)"))
        parameters.append(("pageSize","\(pageSize)"))
        privateAPI(path: "Private/GetClosedFilledOrders", parameters: parameters, completion: orderAPICompletion(orderStatus: nil, primaryCurrencyCode: primaryCurrencyCode, secondaryCurrencyCode: secondaryCurrencyCode, completion: completion))
    }
    
    func orderDetailsAPICompletion(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, completion: @escaping (Result<String>) -> Void) -> ((Result<[String:AnyObject]>) -> Void) {
        let apiCompletion: ((Result<[String:AnyObject]>) -> Void) = {document in
            moc.perform {
                completion(document >>> {[weak self] orderDetailsFromAPI in
                    guard let weakSelf = self else { throw APIError("no self") }
                    
                    guard let orderGuid = orderDetailsFromAPI["OrderGuid"] as? String else { throw APIError("OrderGuid failed to parse") }
                    guard let createdTimestamp = (orderDetailsFromAPI["CreatedTimestampUtc"] as? String)?.dateUTC else { throw APIError("CreatedTimestampUtc failed to parse") }
                    guard let orderType = orderDetailsFromAPI["Type"] as? String else { throw APIError("Type failed to parse") }
                    guard let volumeOrdered = orderDetailsFromAPI["VolumeOrdered"] as? Double else { throw APIError("VolumeOrdered failed to parse") }
                    guard let volumeFilled = orderDetailsFromAPI["VolumeFilled"] as? Double else { throw APIError("VolumeFilled failed to parse") }
                    let price = orderDetailsFromAPI["Price"] as? Double
                    let averagePrice = orderDetailsFromAPI["AvgPrice"] as? Double
                    let reservedAmount = orderDetailsFromAPI["ReservedAmount"] as? Double
                    let status = orderDetailsFromAPI["Status"] as? String
                    guard let primaryCurrencyCode = orderDetailsFromAPI["PrimaryCurrencyCode"] as? String else { throw APIError("PrimaryCurrencyCode failed to parse") }
                    guard let secondaryCurrencyCode = orderDetailsFromAPI["SecondaryCurrencyCode"] as? String else { throw APIError("SecondaryCurrencyCode failed to parse") }
                    
                    guard let order: PrivateOrder = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "orderGuid = %@", orderGuid)) ?? NSEntityDescription.insertNewObject(forEntityName: "PrivateOrder", into: moc) as? PrivateOrder else { throw APIError("insertNewObject PrivateOrder failed") }
                    
                    order.orderGuid = orderGuid
                    order.createdTimestamp = createdTimestamp
                    order.volumeOrdered = volumeOrdered
                    order.volumeFilled = volumeFilled
                    order.price = price ?? 0
                    order.averagePrice = averagePrice ?? 0
                    order.reservedAmount = reservedAmount ?? 0
                    order.status = status
                    order.feePercent = 0
                    
                    guard let orderTypeObject: ValidOrderType = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "type = %@", orderType)) ?? NSEntityDescription.insertNewObject(forEntityName: "ValidOrderType", into: moc) as? ValidOrderType else { throw APIError("insertNewObject ValidOrderType failed") }
                    orderTypeObject.type = orderType
                    
                    guard let primaryCurrencyCodeObject:ValidPrimaryCurrencyCode = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "code = %@", primaryCurrencyCode)) ?? NSEntityDescription.insertNewObject(forEntityName: "ValidPrimaryCurrencyCode", into: moc) as? ValidPrimaryCurrencyCode else { throw APIError("Could not find currency code \(primaryCurrencyCode)") }
                    primaryCurrencyCodeObject.code = primaryCurrencyCode
                    
                    guard let secondaryCurrencyCodeObject:ValidSecondaryCurrencyCode = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "code = %@", secondaryCurrencyCode)) ?? NSEntityDescription.insertNewObject(forEntityName: "ValidSecondaryCurrencyCode", into: moc) as? ValidSecondaryCurrencyCode else { throw APIError("Could not find currency code \(secondaryCurrencyCode)") }
                    secondaryCurrencyCodeObject.code = secondaryCurrencyCode
                    
                    order.relatedOrderType = orderTypeObject
                    order.relatedPrimaryCurrency = primaryCurrencyCodeObject
                    order.relatedSecondaryCurrency = secondaryCurrencyCodeObject
                    
                    // save changes
                    if moc.hasChanges {
                        try moc.save()
                    }
                    
                    return orderGuid
                    }
                )
            }
        }
        return apiCompletion
    }
    
    func GetOrderDetails(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, orderGuid:String, completion: @escaping (Result<String>) -> Void) {
        let parameters = [("orderGuid",orderGuid)]
        privateAPI(path: "Private/GetOrderDetails", parameters: parameters, completion: orderDetailsAPICompletion(moc: moc, completion: completion))
    }
    
    func GetAccounts(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, completion: @escaping (Result<Int>) -> Void) {
        let apiCompletion: ((Result<[[String:AnyObject]]>) -> Void) = {document in
            moc.perform {
                completion(document >>> {[weak self] accountsArrayFromAPI in
                    guard let weakSelf = self else { throw APIError("no self") }
                    
                    var accountsDictionary: [String:Account] = try weakSelf.fetchObjectsAsDictionary(keyFunction: {$0.accountGuid})
                    
                    for accountObject in accountsArrayFromAPI {
                        guard let accountGuid = (accountObject["AccountGuid"] as? String) else { throw APIError("AccountGuid failed to parse") }
                        
                        guard let account = accountsDictionary[accountGuid] ?? NSEntityDescription.insertNewObject(forEntityName: "Account", into: moc) as? Account else { throw APIError("insertNewObject Account failed") }
                        
                        account.accountGuid = accountGuid
                        
                        guard let currencyCode = (accountObject["CurrencyCode"] as? String) else { throw APIError("CurrencyCode failed to parse") }
                        guard let accountStatus = (accountObject["AccountStatus"] as? String) else { throw APIError("AccountStatus failed to parse") }
                        guard let availableBalance = (accountObject["AvailableBalance"] as? Double) else { throw APIError("availableBalance failed to parse") }
                        guard let totalBalance = (accountObject["TotalBalance"] as? Double) else { throw APIError("TotalBalance failed to parse") }
                        
                        guard let currencyCodeObject:ValidCurrencyCode = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "code = %@", currencyCode)) else { throw APIError("Could not find currency code \(currencyCode)") }
                        
                        account.accountStatus = accountStatus
                        account.availableBalance = availableBalance
                        account.totalBalance = totalBalance
                        account.relatedCurrency = currencyCodeObject
                        
                        accountsDictionary[accountGuid] = nil
                    }
                    
                    // remove missing accounts
                    accountsDictionary.values.forEach({moc.delete($0)})
                    
                    // save changes
                    if moc.hasChanges {
                        try moc.save()
                    }

                    return accountsArrayFromAPI.count
                    }
                )
            }
        }
        privateAPI(path: "Private/GetAccounts", parameters: [], completion: apiCompletion)
    }
    
    func GetTransactions(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, accountGuid:String, fromTimestampUtc:Date? = nil, toTimestampUtc:Date? = nil, txTypes:[String]? = nil, pageIndex:Int = 1, pageSize:Int = 25, completion: @escaping (Result<Int>) -> Void) {
        let apiCompletion: ((Result<[String:AnyObject]>) -> Void) = {document in
            moc.perform {
                completion(document >>> {[weak self] transactionsFromAPI in
                    guard let weakSelf = self else { throw APIError("no self") }
                    
                    guard let account: Account = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "accountGuid = %@", accountGuid)) else { throw APIError("Could not find account \(accountGuid)") }
                    
//                    guard let pageSize = transactionsFromAPI["PageSize"] as? Int else { throw APIError("PageSize failed to parse") }
//                    guard let totalItems = transactionsFromAPI["TotalItems"] as? Int else { throw APIError("TotalItems failed to parse") }
                    
                    guard let totalPages = transactionsFromAPI["TotalPages"] as? Int else { throw APIError("TotalPages failed to parse") }
                    guard let data = transactionsFromAPI["Data"] as? [[String:AnyObject]] else { throw APIError("Data failed to parse") }
                    
                    let oldTransactions: [Transaction] = try weakSelf.fetchObjects(satisfying: NSPredicate(format: "relatedAccount.accountGuid = %@", accountGuid))
                    
                    // delete old transactions
                    oldTransactions.forEach({moc.delete($0)})
                    
                    for item in data {
                        guard let balance = item["Balance"] as? Double else { continue }
                        let bitcoinTransactionId = item["BitcoinTransactionId"] as? String
                        let bitcoinTransactionOutputIndex = item["BitcoinTransactionOutputIndex"] as? String
                        let ethereumTransactionId = item["EthereumTransactionId"] as? String
                        let comment = item["Comment"] as? String
                        guard let createdTimestampUtc = (item["CreatedTimestampUtc"] as? String)?.dateUTC else { continue }
                        let credit = item["Credit"] as? Double
                        guard let currencyCode = item["CurrencyCode"] as? String else { continue }
                        let debit = item["Debit"] as? Double
                        guard let settleTimestampUtc = (item["SettleTimestampUtc"] as? String)?.dateUTC else { continue }
                        guard let status = item["Status"] as? String else { continue }
                        guard let type = item["Type"] as? String else { continue }
                        
                        guard let code = account.relatedCurrency?.code, code == currencyCode else { throw APIError("CurrencyCode \(currencyCode) does not match account") }
                        
                        guard let transaction = NSEntityDescription.insertNewObject(forEntityName: "Transaction", into: moc) as? Transaction else { throw APIError("insertNewObject Transaction failed") }
                        
                        guard let transactionType: ValidTransactionType = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "type = %@", type)) ?? NSEntityDescription.insertNewObject(forEntityName: "ValidTransactionType", into: moc) as? ValidTransactionType else { throw APIError("insertNewObject ValidTransactionType failed") }
                        transactionType.type = type
                        
                        transaction.balance = balance
                        transaction.bitcoinTransactionId = bitcoinTransactionId
                        transaction.bitcoinTransactionOutputIndex = bitcoinTransactionOutputIndex
                        transaction.ethereumTransactionId = ethereumTransactionId
                        transaction.comment = comment
                        transaction.createdTimestamp = createdTimestampUtc
                        transaction.credit = credit ?? 0
                        transaction.debit = debit ?? 0
                        transaction.settleTimestamp = settleTimestampUtc
                        transaction.status = status
                        
                        transaction.relatedAccount = account
                        transaction.relatedTransactionType = transactionType
                    }

                    // save changes
                    if moc.hasChanges {
                        try moc.save()
                    }

                    return totalPages
                    }
                )
            }
        }
        
        var parameters = [("accountGuid",accountGuid)]
        if let fromTimestampUtc = fromTimestampUtc {
            parameters.append(("fromTimestampUtc",fromTimestampUtc.iso8601UTC))
        }
        if let toTimestampUtc = toTimestampUtc {
            parameters.append(("toTimestampUtc",toTimestampUtc.iso8601UTC))
        }
        if let txTypes = txTypes {
            parameters.append(("txTypes","\(txTypes)"))
        }
        parameters.append(("pageIndex","\(pageIndex)"))
        parameters.append(("pageSize","\(pageSize)"))
        privateAPI(path: "Private/GetTransactions", parameters: parameters, completion: apiCompletion)
    }
    
    func GetDigitalCurrencyDepositAddress(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, primaryCurrencyCode:String, completion: @escaping (Result<DigitalCurrencyDepositAddress>) -> Void) {
        let apiCompletion: ((Result<[String:AnyObject]>) -> Void) = {document in
            moc.perform {
                completion(document >>> {[weak self] addressFromAPI in
                    guard let weakSelf = self else { throw APIError("no self") }
                    
                    guard let depositAddress = addressFromAPI["DepositAddress"] as? String else { throw APIError("DepositAddress failed to parse") }
                    guard let lastCheckedTimestampUtc = (addressFromAPI["LastCheckedTimestampUtc"] as? String)?.dateUTC else { throw APIError("LastCheckedTimestampUtc failed to parse") }
                    guard let nextUpdateTimestampUtc = (addressFromAPI["NextUpdateTimestampUtc"] as? String)?.dateUTC else { throw APIError("NextUpdateTimestampUtc failed to parse") }
                    
                    guard let address: DigitalCurrencyDepositAddress = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "depositAddress = %@", depositAddress)) ?? NSEntityDescription.insertNewObject(forEntityName: "DigitalCurrencyDepositAddress", into: moc) as? DigitalCurrencyDepositAddress else { throw APIError("insertNewObject DigitalCurrencyDepositAddress failed") }
                    address.depositAddress = depositAddress
                    address.lastCheckedTimestamp = lastCheckedTimestampUtc
                    address.nextUpdateTimestamp = nextUpdateTimestampUtc

                    guard let primaryCurrencyCodeObject:ValidPrimaryCurrencyCode = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "code = %@", primaryCurrencyCode)) ?? NSEntityDescription.insertNewObject(forEntityName: "ValidPrimaryCurrencyCode", into: moc) as? ValidPrimaryCurrencyCode else { throw APIError("Could not find currency code \(primaryCurrencyCode)") }
                    primaryCurrencyCodeObject.code = primaryCurrencyCode
                    
                    address.relatedPrimaryCurrency = primaryCurrencyCodeObject
                    
                    // save changes
                    if moc.hasChanges {
                        try moc.save()
                    }
                    
                    return address
                    }
                )
            }
        }
        let parameters = [("primaryCurrencyCode",primaryCurrencyCode)]
        privateAPI(path: "Private/GetDigitalCurrencyDepositAddress", parameters: parameters, completion: apiCompletion)
    }

    func GetDigitalCurrencyDepositAddresses(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, primaryCurrencyCode:String, pageIndex:Int=1, pageSize:Int = 25, completion: @escaping (Result<Int>) -> Void) {
        let apiCompletion: ((Result<[String:AnyObject]>) -> Void) = {document in
            moc.perform {
                completion(document >>> {[weak self] addressesFromAPI in
                    guard let weakSelf = self else { throw APIError("no self") }
                    
                    guard let data = addressesFromAPI["Data"] as? [[String:AnyObject]] else { throw APIError("Data failed to parse") }
                    
                    var addresses: [String:DigitalCurrencyDepositAddress] = try weakSelf.fetchObjectsAsDictionary(keyFunction: {$0.depositAddress}, predicate: NSPredicate(format: "relatedPrimaryCurrency.code = %@", primaryCurrencyCode))
                    
                    for item in data {
                        guard let depositAddress = item["DepositAddress"] as? String else { throw APIError("DepositAddress failed to parse") }
                        guard let lastCheckedTimestampUtc = (item["LastCheckedTimestampUtc"] as? String)?.dateUTC else { throw APIError("LastCheckedTimestampUtc failed to parse") }
                        guard let nextUpdateTimestampUtc = (item["NextUpdateTimestampUtc"] as? String)?.dateUTC else { throw APIError("NextUpdateTimestampUtc failed to parse") }
                        
                        guard let address: DigitalCurrencyDepositAddress = addresses[depositAddress] ?? NSEntityDescription.insertNewObject(forEntityName: "DigitalCurrencyDepositAddress", into: moc) as? DigitalCurrencyDepositAddress else { throw APIError("insertNewObject DigitalCurrencyDepositAddress failed") }
                        address.depositAddress = depositAddress
                        address.lastCheckedTimestamp = lastCheckedTimestampUtc
                        address.nextUpdateTimestamp = nextUpdateTimestampUtc
                        
                        guard let primaryCurrencyCodeObject:ValidPrimaryCurrencyCode = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "code = %@", primaryCurrencyCode)) ?? NSEntityDescription.insertNewObject(forEntityName: "ValidPrimaryCurrencyCode", into: moc) as? ValidPrimaryCurrencyCode else { throw APIError("Could not find currency code \(primaryCurrencyCode)") }
                        
                        address.relatedPrimaryCurrency = primaryCurrencyCodeObject
                        
                        addresses[depositAddress] = nil
                    }
                    
                    // delete unused
                    addresses.values.forEach({moc.delete($0)})
                    
                    // save changes
                    if moc.hasChanges {
                        try moc.save()
                    }
                    
                    return data.count
                    }
                )
            }
        }
        let parameters = [("primaryCurrencyCode",primaryCurrencyCode),
                          ("pageIndex","\(pageIndex)"),
                          ("pageSize","\(pageSize)")]
        privateAPI(path: "Private/GetDigitalCurrencyDepositAddresses", parameters: parameters, completion: apiCompletion)
    }
    
    func SynchDigitalCurrencyDepositAddressWithBlockchain(depositAddress:String, primaryCurrencyCode:String?) {
        let completion: ((Result<[String:AnyObject]>) -> Void) = {result in print(result)}
        var parameters = [("depositAddress",depositAddress)]
        if let primaryCurrencyCode = primaryCurrencyCode {
            parameters.append(("primaryCurrencyCode",primaryCurrencyCode))
        }
        privateAPI(path: "Private/SynchDigitalCurrencyDepositAddressWithBlockchain", parameters: parameters, completion: completion)
    }
    
    func WithdrawDigitalCurrency(amount:String, withdrawalAddress:String, comment:String, primaryCurrencyCode:String) {
        let completion: ((Result<[String:AnyObject]>) -> Void) = {result in print(result)}
        let parameters = [("amount",amount),
                          ("withdrawalAddress",withdrawalAddress),
                          ("comment",comment),
                          ("primaryCurrencyCode",primaryCurrencyCode)]
        privateAPI(path: "Private/WithdrawDigitalCurrency", parameters: parameters, completion: completion)
    }
    
    func RequestFiatWithdrawal(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, secondaryCurrencyCode:String, withdrawalAmount:String, withdrawalBankAccountName:String, comment:String, completion: @escaping (Result<FiatWithdrawal>) -> Void) {
        let apiCompletion: ((Result<[String:AnyObject]>) -> Void) = {document in
            moc.perform {
                completion(document >>> {[weak self] fiatRequestFromAPI in
                    guard let weakSelf = self else { throw APIError("no self") }
                    
                    guard let fiatRequestGuid = fiatRequestFromAPI["FiatWithdrawalRequestGuid"] as? String else { throw APIError("FiatWithdrawalRequestGuid failed to parse") }
                    guard let accountGuid = fiatRequestFromAPI["AccountGuid"] as? String else { throw APIError("AccountGuid failed to parse") }
                    guard let status = fiatRequestFromAPI["Status"] as? String else { throw APIError("Status failed to parse") }
                    guard let createdTimestamp = (fiatRequestFromAPI["CreatedTimestampUtc"] as? String)?.dateUTC else { throw APIError("CreatedTimestampUtc failed to parse") }
                    guard let totalWithdrawalAmount = fiatRequestFromAPI["TotalWithdrawalAmount"] as? Double else { throw APIError("TotalWithdrawalAmount failed to parse") }
                    guard let feeAmount = fiatRequestFromAPI["FeeAmount"] as? Double else { throw APIError("FeeAmount failed to parse") }
                    guard let currency = fiatRequestFromAPI["Currency"] as? String else { throw APIError("Currency failed to parse") }
                    
                    guard let fiatWithdrawal = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "fiatWithdrawalRequestGuid = %@", fiatRequestGuid)) ?? NSEntityDescription.insertNewObject(forEntityName: "FiatWithdrawal", into: moc) as? FiatWithdrawal else { throw APIError("insertNewObject FiatWithdrawal failed") }
                    
                    fiatWithdrawal.fiatWithdrawalRequestGuid = fiatRequestGuid
                    fiatWithdrawal.accountGuid = accountGuid
                    fiatWithdrawal.status = status
                    fiatWithdrawal.createdTimestamp = createdTimestamp
                    fiatWithdrawal.totalWithdrawalAmount = totalWithdrawalAmount
                    fiatWithdrawal.feeAmount = feeAmount
                    
                    guard let secondaryCurrencyCodeObject:ValidSecondaryCurrencyCode = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "code = %@", currency)) ?? NSEntityDescription.insertNewObject(forEntityName: "ValidSecondaryCurrencyCode", into: moc) as? ValidSecondaryCurrencyCode else { throw APIError("Could not find currency code \(currency)") }
                    secondaryCurrencyCodeObject.code = secondaryCurrencyCode

                    fiatWithdrawal.relatedSecondaryCurrency = secondaryCurrencyCodeObject
                    
                    // save changes
                    if moc.hasChanges {
                        try moc.save()
                    }
                    
                    return fiatWithdrawal
                    }
                )
            }
        }
        let parameters = [("secondaryCurrencyCode",secondaryCurrencyCode),
                          ("withdrawalAmount",withdrawalAmount),
                          ("withdrawalBankAccountName",withdrawalBankAccountName),
                          ("comment",comment)]
        privateAPI(path: "Private/RequestFiatWithdrawal", parameters: parameters, completion: apiCompletion)
    }
    
    func GetTrades(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, pageIndex:Int=1, pageSize:Int = 25, completion: @escaping (Result<Int>) -> Void) {
        let apiCompletion: ((Result<[String:AnyObject]>) -> Void) = {document in
            moc.perform {
                completion(document >>> {[weak self] tradesFromAPI in
                    guard let weakSelf = self else { throw APIError("no self") }
                    
                    guard let trades = tradesFromAPI["Data"] as? [[String:AnyObject]] else { throw APIError("Data failed to parse") }
                    
                    var tradesDictionary: [String:Trade] = try weakSelf.fetchObjectsAsDictionary(keyFunction: {$0.tradeGuid})
                    
                    for tradeObject in trades {
                        guard let tradeGuid = (tradeObject["TradeGuid"] as? String) else { throw APIError("TradeGuid failed to parse") }

                        guard let trade = tradesDictionary[tradeGuid] ?? NSEntityDescription.insertNewObject(forEntityName: "Trade", into: moc) as? Trade else { throw APIError("insertNewObject Trade failed") }
                        
                        guard let tradeTimestampUtc = (tradeObject["TradeTimestampUtc"] as? String)?.dateUTC else { throw APIError("TradeTimestampUtc failed to parse") }
                        guard let orderGuid = (tradeObject["OrderGuid"] as? String) else { throw APIError("OrderGuid failed to parse") }
                        guard let orderType = (tradeObject["OrderType"] as? String) else { throw APIError("OrderType failed to parse") }
                        guard let orderTimestampUtc = (tradeObject["OrderTimestampUtc"] as? String)?.dateUTC else { throw APIError("OrderTimestampUtc failed to parse") }
                        guard let volumeTraded = (tradeObject["VolumeTraded"] as? Double) else { throw APIError("VolumeTraded failed to parse") }
                        guard let price = (tradeObject["Price"] as? Double) else { throw APIError("Price failed to parse") }
                        guard let primaryCurrencyCode = (tradeObject["PrimaryCurrencyCode"] as? String) else { throw APIError("PrimaryCurrencyCode failed to parse") }
                        guard let secondaryCurrencyCode = (tradeObject["SecondaryCurrencyCode"] as? String) else { throw APIError("SecondaryCurrencyCode failed to parse") }
                        
                        guard let orderTypeObject:ValidOrderType = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "type = %@", orderType)) ?? NSEntityDescription.insertNewObject(forEntityName: "ValidOrderType", into: moc) as? ValidOrderType else { throw APIError("Could not find order type \(orderType)") }
                        orderTypeObject.type = orderType
                        
                        guard let primaryCurrencyCodeObject:ValidPrimaryCurrencyCode = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "code = %@", primaryCurrencyCode)) ?? NSEntityDescription.insertNewObject(forEntityName: "ValidPrimaryCurrencyCode", into: moc) as? ValidPrimaryCurrencyCode else { throw APIError("Could not find currency code \(primaryCurrencyCode)") }
                        primaryCurrencyCodeObject.code = primaryCurrencyCode
                        
                        guard let secondaryCurrencyCodeObject:ValidSecondaryCurrencyCode = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "code = %@", secondaryCurrencyCode)) ?? NSEntityDescription.insertNewObject(forEntityName: "ValidSecondaryCurrencyCode", into: moc) as? ValidSecondaryCurrencyCode else { throw APIError("Could not find currency code \(secondaryCurrencyCode)") }
                        secondaryCurrencyCodeObject.code = secondaryCurrencyCode
                        
                        trade.tradeGuid = tradeGuid
                        trade.tradeTimestamp = tradeTimestampUtc
                        trade.orderGuid = orderGuid
                        trade.orderTimestamp = orderTimestampUtc
                        trade.volumeTraded = volumeTraded
                        trade.price = price
                        
                        trade.relatedOrderType = orderTypeObject
                        trade.relatedPrimaryCurrency = primaryCurrencyCodeObject
                        trade.relatedSecondaryCurrency = secondaryCurrencyCodeObject
                        
                        tradesDictionary[tradeGuid] = nil
                    }
                    
                    // remove missing trades
                    tradesDictionary.values.forEach({moc.delete($0)})
                    
                    // save changes
                    if moc.hasChanges {
                        try moc.save()
                    }
                    
                    return tradesFromAPI.count
                    }
                )
            }
        }
        let parameters = [("pageIndex","\(pageIndex)"),
                          ("pageSize","\(pageSize)")]
        privateAPI(path: "Private/GetTrades", parameters: parameters, completion: apiCompletion)
    }

    func GetBrokerageFees(moc:NSManagedObjectContext = IRCoreDataStack.shared.mainContext, completion: @escaping (Result<Int>) -> Void) {
        let apiCompletion: ((Result<[[String:AnyObject]]>) -> Void) = {document in
            moc.perform {
                completion(document >>> {[weak self] feesFromAPI in
                    guard let weakSelf = self else { throw APIError("no self") }
                    
                    var feesDictionary: [String:BrokerageFee] = try weakSelf.fetchObjectsAsDictionary(keyFunction: {$0.relatedCurrency?.code})
                    
                    for feeDictionary in feesFromAPI {
                        guard let code = feeDictionary["CurrencyCode"] as? String else { throw APIError("CurrencyCode failed to parse") }
                        guard let fee = feeDictionary["Fee"] as? Double else { throw APIError("Fee failed to parse") }
                        
                        guard let currencyCodeObject:ValidCurrencyCode = try weakSelf.fetchFirstObject(satisfying: NSPredicate(format: "code = %@", code)) ?? NSEntityDescription.insertNewObject(forEntityName: "ValidCurrencyCode", into: moc) as? ValidCurrencyCode else { throw APIError("Could not find currency code \(code)") }
                        currencyCodeObject.code = code
                        
                        guard let feeObject = feesDictionary[code] ?? NSEntityDescription.insertNewObject(forEntityName: "BrokerageFee", into: moc) as? BrokerageFee else { throw APIError("BrokerageFee failed to insert") }
                        
                        feeObject.relatedCurrency = currencyCodeObject
                        feeObject.fee = fee
                        
                        feesDictionary[code] = nil
                    }
                    
                    // delete unused
                    feesDictionary.values.forEach({moc.delete($0)})
                    
                    // save changes
                    if moc.hasChanges {
                        try moc.save()
                    }
                    
                    return feesFromAPI.count
                    }
                )
            }
        }
        privateAPI(path: "Private/GetBrokerageFees", parameters: [], completion: apiCompletion)
    }

    func privateAPI<T>(path:String, parameters: [(String,String)], completion: @escaping (Result<T>) -> Void) {
        IRLocalAuthentication.shared.auth {[weak self] in
            guard let weakSelf = self else { return }
            let apiSecret = weakSelf.apiSecret
            let apiKey = weakSelf.apiKey
            let baseUrl = weakSelf.baseUrl
            let nonce = weakSelf.nonce
            let signature = String.signature(apiSecret: apiSecret, url: baseUrl + path, apiKey: apiKey, nonce: nonce, parameters: parameters)
            weakSelf.postWith(path: path, parameters: [("apiKey",apiKey),
                                                       ("nonce","\(nonce)"),
                                                       ("signature",signature)] + parameters,
                     completion: completion)
        }
    }
    
    // MARK: - JSON helper stuff
    
    /**
     API HTTP post request.
     - parameter path: The name of the API.
     - parameter parameters: The parameters to pass to the API.
     - parameter authorization: The access token.
     - parameter completion: A completion block. On success, contains API returned result.
     */
    func postWith<T>(path: String, parameters: [(String,String)]? = nil, completion: @escaping (Result<T>) -> Void) {
        
        var payload: Data? = nil
        if let parameters = parameters {
            let jsonString = "{" + parameters.map{"\"\($0.0)\":\"\($0.1)\""}.joined(separator: ",") + "}"
            payload = jsonString.data(using: .utf8)
        }
        jsonRequest(httpMethod: .post, path: path, payload: payload, completion: completion)
    }
    
    /**
     API HTTP get request.
     - parameter path: The name of the API.
     - parameter parameters: The parameters to pass to the API.
     - parameter authorization: The access token.
     - parameter completion: A completion block. On success, contains API returned result.
     */
    func getWith<T>(path: String, parameters: [String:String]? = nil, completion: @escaping (Result<T>) -> Void) {
        
        let parameterString = parameters?.map({(key,value) in key + "=" + value}).joined(separator: "&")
        let payload = parameterString?.data(using: .utf8)
        jsonRequest(httpMethod: .get, path: path, payload: payload, completion: completion)
    }
    
    /**
     API HTTP request expecting JSON result. Handles both GET and POST requests.
     - parameter path: The name of the API.
     - parameter parameters: The parameters to pass to the API.
     - parameter authorization: The access token.
     - parameter httpMethod: Use GET or POST.
     - parameter completion: A completion block. On success, contains API returned result.
     */
    func jsonRequest<T>(httpMethod:IRHTTPMethod, path: String, payload:Data? = nil, completion: @escaping (Result<T>) -> Void) {
        
        let urlString = baseUrl + path
        guard let endPoint = URL(string: urlString) else { return }
        
        networkClient.request(method: httpMethod, url: endPoint, payload: payload, completion: {(statusCode, data, error) in
            
            do {
                if let error = error {
                    completion(.failure(error))
                }
                else if let data = data, let document = try JSONSerialization.jsonObject(with: data, options: []) as? T {
                    completion(.success(document))
                }
                else {
                    completion(.failure(nil))
                }
            }
            catch {
                completion(.failure(error))
            }
        })
    }
}
