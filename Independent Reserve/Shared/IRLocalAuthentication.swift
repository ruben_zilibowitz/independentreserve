//
//  IRLocalAuthentication.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 25/5/18.
//  Copyright © 2018 Ruben Zilibowitz. All rights reserved.
//

import Foundation
import LocalAuthentication

class IRLocalAuthentication {
    
    // singleton
    private init() {}
    static let shared = IRLocalAuthentication()
    
    static let IRFailedAuthentication = NSNotification.Name(rawValue: "IRFailedAuthentication")
    
    var isAuthenticated: Bool = false
    
    func auth(function: @escaping () -> Void) {
        if self.isAuthenticated {
            function()
        }
        else {
            let myContext = LAContext()
            let myLocalizedReasonString = "Access to private API"
            
            var authError: NSError?
            if #available(iOS 8.0, macOS 10.12.1, *) {
                if myContext.canEvaluatePolicy(.deviceOwnerAuthentication, error: &authError) {
                    myContext.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: myLocalizedReasonString) {[weak self] success, evaluateError in
                        guard let weakSelf = self else { return }
                        if success {
                            // User authenticated successfully, take appropriate action
                            weakSelf.isAuthenticated = true
                            function()
                        } else {
                            // User did not authenticate successfully, look at error and take appropriate action
                            NSLog(myLocalizedReasonString)
                            NotificationCenter.default.post(name: IRLocalAuthentication.IRFailedAuthentication, object: myLocalizedReasonString)
                        }
                    }
                } else {
                    // Could not evaluate policy; look at authError and present an appropriate message to user
                    let message = authError?.localizedDescription ?? "error"
                    NSLog(message)
                    NotificationCenter.default.post(name: IRLocalAuthentication.IRFailedAuthentication, object: message)
                }
            } else {
                // Fallback on earlier versions
                fatalError("iOS version not ok")
            }
        }
    }
}
