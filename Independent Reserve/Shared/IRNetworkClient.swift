//
//  IRNetworkClient.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 21/11/17.
//  Copyright © 2017 Ruben Zilibowitz. All rights reserved.
//

import Foundation

typealias IRNetworkClientCompletion = (_ statusCode: Int?, _ data: Data?, _ error: NSError?) -> Void

enum IRHTTPMethod:String {
    case get = "GET"
    case post = "POST"
}

protocol IRNetworkClient {
    func request(method: IRHTTPMethod, url: URL, payload: Data?, completion: @escaping IRNetworkClientCompletion)
}

class IRHTTPClient: IRNetworkClient {
    let urlSession: URLSession
    var headers: [String: String] = ["Accept": "application/json", "Content-Type": "application/json"]
    
    static let IRNetworkWarning = NSNotification.Name(rawValue: "IRNetworkWarning")
    
    init(session: URLSession) {
        self.urlSession = session
    }
    
    convenience init() {
        let sessionConfiguration = URLSessionConfiguration.default
        self.init(session: URLSession(configuration: sessionConfiguration))
    }
    
    func setHeader(_ header: String, to value: String) {
        headers[header] = value
    }
    
    func removeHeader(_ header: String) {
        headers.removeValue(forKey: header)
    }
    
    func buildRequest(_ method: IRHTTPMethod, url: URL, payload: Data?) -> URLRequest {
        
        var payload = payload
        var url = url
        
        switch method {
        case .get:
            let absoluteString = url.absoluteString
            if let payload = payload,
                let ext = String(data: payload, encoding: .utf8),
                let getUrl = URL(string: absoluteString + "?" + ext) {
                url = getUrl
            }
            payload = nil
        case .post:
            break
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        
        for (key, value) in headers {
            request.setValue(value, forHTTPHeaderField: key)
        }
        
        if let payload = payload {
            request.httpBody = payload
        }
        
        return request
    }
    
    func request(method: IRHTTPMethod, url: URL, payload: Data?, completion: @escaping IRNetworkClientCompletion) {
        let request = buildRequest(method, url: url, payload: payload)
        
        let task = urlSession.dataTask(with: request, completionHandler: { data, response, networkError in
            let response = (response as? HTTPURLResponse)
            
            if let error = networkError {
                NSLog("Network warning - %@", "\(request.url!) - \(error.localizedDescription)")
                NotificationCenter.default.post(name: IRHTTPClient.IRNetworkWarning, object: error)
            }
            else if let statusCode = response?.statusCode , 200 ... 299 ~= statusCode {
                NSLog("Network success - %@", "\(statusCode): \(request.url!) – (\(data!.count) bytes)")
            }
            else {
                NSLog("Network warning - %@", "\(response!.statusCode): \(request.url!) – (\(data!.count) bytes)")
                NotificationCenter.default.post(name: IRHTTPClient.IRNetworkWarning, object: response!.statusCode)
            }
            
            completion(response?.statusCode, data, networkError as NSError?)
        })
        
        task.resume()
    }
}

