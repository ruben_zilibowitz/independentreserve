//
//  String+DateUTC.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 23/11/17.
//  Copyright © 2017 Ruben Zilibowitz. All rights reserved.
//

import Foundation

extension String {
    var dateUTC: Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZ"
        formatter.timeZone = TimeZone(abbreviation: "utc")
        let localDate = formatter.date(from: self)
        return localDate
    }
    
    var dateShortUTC: Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        formatter.timeZone = TimeZone(abbreviation: "utc")
        let localDate = formatter.date(from: self)
        return localDate
    }
}
