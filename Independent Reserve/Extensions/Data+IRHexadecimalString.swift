//
//  Data+IRHexadecimalString.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 21/11/17.
//  Copyright © 2017 Ruben Zilibowitz. All rights reserved.
//

import Foundation

extension Data {
    var hexadecimalString: String {
        return map { String(format: "%02hhx", $0) }.joined()
    }
}
