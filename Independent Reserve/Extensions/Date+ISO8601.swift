//
//  Date+ISO8601.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 22/11/17.
//  Copyright © 2017 Ruben Zilibowitz. All rights reserved.
//

import Foundation

extension Date {
    var iso8601UTC: String {
        let UTC = TimeZone(abbreviation: "UTC")!
        return ISO8601DateFormatter.string(from: self, timeZone: UTC, formatOptions: [])
    }
}
