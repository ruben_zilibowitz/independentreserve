//
//  String+IRSignature.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 21/11/17.
//  Copyright © 2017 Ruben Zilibowitz. All rights reserved.
//

import Foundation

extension String {
    static func signature(apiSecret:String, url:String, apiKey:String, nonce:Int, parameters:[(String,String)]) -> String {
        let apiSecretData = apiSecret.data(using: .utf8)!
        let allParameters = [("apiKey",apiKey),("nonce","\(nonce)")] + parameters
        let messageData = (url + "," + allParameters.map{$0.0 + "=" + $0.1}.joined(separator: ",")).data(using: .utf8)!
        var hash = Data(capacity: Int(CC_SHA256_DIGEST_LENGTH))
        hash.resetBytes(in: 0 ..< Int(CC_SHA256_DIGEST_LENGTH))
        
        apiSecretData.withUnsafeBytes({apiSecretDataBytes in
            messageData.withUnsafeBytes({messageDataBytes in
                hash.withUnsafeMutableBytes({hashMutableBytes in
                    CCHmac(CCHmacAlgorithm(kCCHmacAlgSHA256), apiSecretDataBytes, apiSecretData.count, messageDataBytes, messageData.count, hashMutableBytes)
                })
            })
        })
        
        return hash.hexadecimalString.uppercased()
    }
}
