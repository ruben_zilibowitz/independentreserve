//
//  CurrencyTableViewController.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 11/12/17.
//  Copyright © 2017 Ruben Zilibowitz. All rights reserved.
//

import UIKit
import CoreData

protocol UsesCurrency {
    var primaryCurrency: ValidPrimaryCurrencyCode? { get set }
    var secondaryCurrency: ValidSecondaryCurrencyCode? { get set }
}

class CurrencyTableViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    
    @IBOutlet weak var nextBarButton: UIBarButtonItem?
    
    @IBInspectable var isPrimaryAvailable: Bool = true
    @IBInspectable var isSecondaryAvailable: Bool = true
    
    @IBInspectable var isAllPrimaryAvailable: Bool = false
    @IBInspectable var isAllSecondaryAvailable: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        nextBarButton?.isEnabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var n = fetchedResultsController.sections?[section].numberOfObjects ?? 0
        let isPrimarySection = fetchedResultsController.sectionIndexTitles[section] == "1"
        if isPrimarySection && isAllPrimaryAvailable {
            n += 1
        }
        if !isPrimarySection && isAllSecondaryAvailable {
            n += 1
        }
        return n
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrencyCell", for: indexPath)
        
        if let sectionRows = fetchedResultsController.sections?[indexPath.section].numberOfObjects,
            indexPath.row < sectionRows {
            cell.textLabel?.text = fetchedResultsController.object(at: indexPath).code
        }
        else {
            cell.textLabel?.text = "All of the above"
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return fetchedResultsController.sectionIndexTitles[section] == "1" ? "Primary" : "Secondary"
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        for vcip in tableView.indexPathsForSelectedRows ?? [] {
            if vcip.section == indexPath.section && vcip.item != indexPath.item {
                tableView.deselectRow(at: vcip, animated: false)
            }
        }
        return indexPath
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        nextBarButton?.isEnabled = (tableView.indexPathsForSelectedRows?.count == fetchedResultsController.sections?.count)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if var currencyVC = segue.destination as? UsesCurrency {
            tableView.indexPathsForSelectedRows?.forEach({ip in
                if let sectionRows = fetchedResultsController.sections?[ip.section].numberOfObjects,
                    ip.row < sectionRows {
                    let currencyCode = fetchedResultsController.object(at: ip)
                    if let primaryCurrencyCode = currencyCode as? ValidPrimaryCurrencyCode {
                        currencyVC.primaryCurrency = primaryCurrencyCode
                    }
                    else if let secondaryCurrencyCode = currencyCode as? ValidSecondaryCurrencyCode {
                        currencyVC.secondaryCurrency = secondaryCurrencyCode
                    }
                }
                else {
                    let isPrimarySection = fetchedResultsController.sectionIndexTitles[ip.section] == "1"
                    if isPrimarySection {
                        currencyVC.primaryCurrency = nil
                    }
                    else {
                        currencyVC.secondaryCurrency = nil
                    }
                }
            })
        }
    }
    
    // MARK: - Fetched results controller
    
    var fetchedResultsController: NSFetchedResultsController<ValidCurrencyCode> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest: NSFetchRequest<ValidCurrencyCode> = ValidCurrencyCode.fetchRequest()
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        let sortDescriptor1 = NSSortDescriptor(key: "isPrimary", ascending: false, selector: nil)
        let sortDescriptor2 = NSSortDescriptor(key: "code", ascending: true, selector: #selector(NSString.caseInsensitiveCompare))
        
        fetchRequest.sortDescriptors = [sortDescriptor1, sortDescriptor2]
        
        if isPrimaryAvailable && !isSecondaryAvailable {
            fetchRequest.predicate = NSPredicate(format: "isPrimary = TRUE")
        }
        else if !isPrimaryAvailable && isSecondaryAvailable {
            fetchRequest.predicate = NSPredicate(format: "isPrimary = FALSE")
        }
        else if !isPrimaryAvailable && !isSecondaryAvailable {
            fetchRequest.predicate = NSPredicate(format: "FALSEPREDICATE")
        }
        
        let managedObjectContext = IRCoreDataStack.shared.persistentContainer.viewContext
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: "isPrimary", cacheName: nil)
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            let nserror = error as NSError
            presentError(message: "Unresolved error \(nserror), \(nserror.userInfo)")
        }
        
        return _fetchedResultsController!
    }
    var _fetchedResultsController: NSFetchedResultsController<ValidCurrencyCode>? = nil

    // MARK: - FRC delegate
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView?.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView?.insertSections(IndexSet(integer: sectionIndex), with: .automatic)
        case .delete:
            tableView?.deleteSections(IndexSet(integer: sectionIndex), with: .automatic)
        default:
            return
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            tableView?.insertRows(at: [newIndexPath!], with: .automatic)
        case .delete:
            tableView?.deleteRows(at: [indexPath!], with: .automatic)
        case .update:
            if let cell = tableView.cellForRow(at: indexPath!) {
                cell.textLabel?.text = fetchedResultsController.object(at: indexPath!).code
            }
        case .move:
            if let cell = tableView.cellForRow(at: indexPath!) {
                cell.textLabel?.text = fetchedResultsController.object(at: indexPath!).code
            }
            tableView?.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView?.endUpdates()
    }
}
