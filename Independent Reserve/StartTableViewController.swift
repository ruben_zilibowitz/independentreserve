//
//  StartTableViewController.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 29/12/17.
//  Copyright © 2017 Ruben Zilibowitz. All rights reserved.
//

import UIKit
import MobileCoreServices
import MessageUI

class StartTableViewController: UITableViewController, MFMailComposeViewControllerDelegate {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        NotificationCenter.default.addObserver(self, selector: #selector(failedAuthentication), name: IRLocalAuthentication.IRFailedAuthentication, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(networkWarning), name: IRHTTPClient.IRNetworkWarning, object: nil)
    }
    
    @objc func failedAuthentication(note:Notification) {
        DispatchQueue.main.async {[weak self] in
            guard let weakSelf = self else { return }
            weakSelf.navigationController?.popToRootViewController(animated: true)
            if let message = note.object as? String {
                weakSelf.presentError(message: message)
            }
        }
    }
    
    @objc func networkWarning(note:Notification) {
        DispatchQueue.main.async {[weak self] in
            guard let weakSelf = self else { return }
            let error = note.object as? Error
            let message = error?.localizedDescription ?? "Network warning"
            weakSelf.navigationController?.popToRootViewController(animated: true)
            weakSelf.presentError(message: message)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        IRAPIService.shared.GetValidPrimaryCurrencyCodes(completion: {_ in
            DispatchQueue.main.async {
                IRAPIService.shared.GetValidSecondaryCurrencyCodes(completion: {_ in
                })
            }
        })
        
        IRAPIService.shared.GetValidLimitOrderTypes(completion: {_ in
            DispatchQueue.main.async {
                IRAPIService.shared.GetValidMarketOrderTypes(completion: {_ in
                })
            }
        })
        
        IRAPIService.shared.GetValidTransactionTypes(completion: {_ in})
    }
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        return identifier
    }
    
    func sendSupportEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["myreserve@zilibowitzproductions.com"])
            mail.setSubject("Support request")
            mail.setMessageBody("<p>Device model: \(modelName)<br>App version: \(Bundle.main.versionString)<br>iOS version: \(UIDevice.current.systemVersion)<br>Please enter your query below this line<br>------------</p>", isHTML: true)
            
            present(mail, animated: true)
        } else {
            presentError(message: "MFMailComposeViewController failed")
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 2 {
            let apiKeySecretCheck = IRAPIService.shared.apiKey.count > 0 && IRAPIService.shared.apiSecret.count > 0
            guard apiKeySecretCheck else {
                presentError(message: "You must enter an API key and secret to access the private API. Do this in Settings.")
                return
            }
            IRLocalAuthentication.shared.auth {
                DispatchQueue.main.async {[weak self] in
                    self?.performSegue(withIdentifier: "showPrivateAPI", sender: self)
                }
            }
        }
        else if indexPath.row == 3 {
            sendSupportEmail()
        }
    }
    
    // MARK: - MFMailComposeViewControllerDelegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        if let error = error {
            presentError(message: error.localizedDescription)
        }
        controller.dismiss(animated: true)
    }
}
