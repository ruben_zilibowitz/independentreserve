//
//  BrokerageFeeTableViewCell.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 20/5/18.
//  Copyright © 2018 Ruben Zilibowitz. All rights reserved.
//

import UIKit

class BrokerageFeeTableViewCell: UITableViewCell {

    var fee: BrokerageFee? {
        didSet {
            configureCell()
        }
    }
    
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var feeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell() {
        guard let fee = self.fee else {
            codeLabel.text = nil
            feeLabel.text = nil
            return
        }
        
        codeLabel.text = fee.relatedCurrency?.code
        feeLabel.text = "\(fee.fee)"
    }
}
