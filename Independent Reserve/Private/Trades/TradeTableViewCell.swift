//
//  TradeTableViewCell.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 20/5/18.
//  Copyright © 2018 Ruben Zilibowitz. All rights reserved.
//

import UIKit

class TradeTableViewCell: UITableViewCell {

    var trade: Trade? {
        didSet {
            configureCell()
        }
    }
    
    @IBOutlet weak var tradeGuidLabel: UILabel!
    @IBOutlet weak var tradeTimestampLabel: UILabel!
    @IBOutlet weak var orderGuidLabel: UILabel!
    @IBOutlet weak var orderTypeLabel: UILabel!
    @IBOutlet weak var orderTimestampLabel: UILabel!
    @IBOutlet weak var volumeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var primaryCurrencyLabel: UILabel!
    @IBOutlet weak var secondaryCurrencyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell() {
        guard let trade = self.trade else {
            tradeGuidLabel.text = nil
            tradeTimestampLabel.text = nil
            orderGuidLabel.text = nil
            orderTypeLabel.text = nil
            orderTimestampLabel.text = nil
            volumeLabel.text = nil
            priceLabel.text = nil
            primaryCurrencyLabel.text = nil
            secondaryCurrencyLabel.text = nil
            return
        }
        
        tradeGuidLabel.text = trade.tradeGuid
        tradeTimestampLabel.text = trade.tradeTimestamp?.shortString
        orderGuidLabel.text = trade.orderGuid
        orderTypeLabel.text = trade.relatedOrderType?.type
        orderTimestampLabel.text = trade.orderTimestamp?.shortString
        volumeLabel.text = "\(trade.volumeTraded)"
        priceLabel.text = "\(trade.price)"
        primaryCurrencyLabel.text = trade.relatedPrimaryCurrency?.code
        secondaryCurrencyLabel.text = trade.relatedSecondaryCurrency?.code
    }
}
