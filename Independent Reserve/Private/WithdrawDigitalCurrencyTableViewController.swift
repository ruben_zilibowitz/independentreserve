//
//  WithdrawDigitalCurrencyTableViewController.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 23/5/18.
//  Copyright © 2018 Ruben Zilibowitz. All rights reserved.
//

import UIKit
import CoreData

class WithdrawDigitalCurrencyTableViewController: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource, NSFetchedResultsControllerDelegate {

    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var currencyPicker: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func tappedExecute(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Are you sure?", message: "Please make sure the withdrawal address is correct. This action cannot be undone.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Continue", style: .`default`, handler: {[weak self] action in
            guard let weakSelf = self else { return }
            guard let amount = weakSelf.amountTextField.text else { return }
            guard let withdrawalAddress = weakSelf.addressTextField.text else { return }
            guard let comment = weakSelf.commentTextField.text else { return }
            guard let primaryCurrencyCode = weakSelf.primaryCurrency_fetchedResultsController.object(at: IndexPath(row: weakSelf.currencyPicker.selectedRow(inComponent: 0), section: 0)).code else { return }
            IRAPIService.shared.WithdrawDigitalCurrency(amount: amount, withdrawalAddress: withdrawalAddress, comment: comment, primaryCurrencyCode: primaryCurrencyCode)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return primaryCurrency_fetchedResultsController.sections?.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return primaryCurrency_fetchedResultsController.sections?[component].numberOfObjects ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return primaryCurrency_fetchedResultsController.object(at: IndexPath(row: row, section: component)).code
    }
    
    // MARK: - Fetched results controller
    
    var primaryCurrency_fetchedResultsController: NSFetchedResultsController<ValidPrimaryCurrencyCode> {
        if _primaryCurrency_fetchedResultsController != nil {
            return _primaryCurrency_fetchedResultsController!
        }
        
        let fetchRequest: NSFetchRequest<ValidPrimaryCurrencyCode> = ValidPrimaryCurrencyCode.fetchRequest()
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        let sortDescriptor = NSSortDescriptor(key: "code", ascending: true, selector: #selector(NSString.caseInsensitiveCompare))
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let managedObjectContext = IRCoreDataStack.shared.persistentContainer.viewContext
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        aFetchedResultsController.delegate = self
        _primaryCurrency_fetchedResultsController = aFetchedResultsController
        
        do {
            try _primaryCurrency_fetchedResultsController!.performFetch()
        } catch {
            let nserror = error as NSError
            presentError(message: "Unresolved error \(nserror), \(nserror.userInfo)")
        }
        
        return _primaryCurrency_fetchedResultsController!
    }
    var _primaryCurrency_fetchedResultsController: NSFetchedResultsController<ValidPrimaryCurrencyCode>? = nil

    // MARK: - FRC delegate
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        currencyPicker.reloadAllComponents()
    }
}
