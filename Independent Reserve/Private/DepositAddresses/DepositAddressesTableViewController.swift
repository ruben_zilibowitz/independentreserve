//
//  DepositAddressesTableViewController.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 20/5/18.
//  Copyright © 2018 Ruben Zilibowitz. All rights reserved.
//

import UIKit
import CoreData

class DepositAddressesTableViewController: UITableViewController, NSFetchedResultsControllerDelegate, UsesCurrency {
    
    var primaryCurrency: ValidPrimaryCurrencyCode?
    var secondaryCurrency: ValidSecondaryCurrencyCode?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        
        navigationItem.title = "\(primaryCurrency?.code ?? "?") Deposit Addresses"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let code = primaryCurrency?.code else { return }
        
        IRAPIService.shared.GetDigitalCurrencyDepositAddresses(primaryCurrencyCode: code, completion: {result in
            switch result {
            case .failure(let error):
                DispatchQueue.main.async {[weak self] in self?.presentError(message: error?.localizedDescription ?? "error")}
            case .success(let count):
                NSLog("%d", count)
            }
        })
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DepositAddressTableViewCell", for: indexPath) as? DepositAddressTableViewCell else { return UITableViewCell() }
        
        cell.address = fetchedResultsController.object(at: indexPath)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let address = fetchedResultsController.object(at: indexPath)
        guard let depositAddress = address.depositAddress else { return }
        UIPasteboard.general.string = depositAddress
        
        let note = UIAlertController(title: "Copied address to clipboard", message: nil, preferredStyle: .actionSheet)
        note.addAction(UIAlertAction(title: "OK", style: .`default`, handler: nil))
        self.present(note, animated: true, completion: nil)
    }
    
    // MARK: - Fetched results controller
    
    var fetchedResultsController: NSFetchedResultsController<DigitalCurrencyDepositAddress> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest: NSFetchRequest<DigitalCurrencyDepositAddress> = DigitalCurrencyDepositAddress.fetchRequest()
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        let sortDescriptor = NSSortDescriptor(key: "lastCheckedTimestamp", ascending: false, selector: nil)
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        fetchRequest.predicate = NSPredicate(format: "relatedPrimaryCurrency.code = %@", primaryCurrency?.code ?? "")
        
        let managedObjectContext = IRCoreDataStack.shared.persistentContainer.viewContext
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            let nserror = error as NSError
            presentError(message: "Unresolved error \(nserror), \(nserror.userInfo)")
        }
        
        return _fetchedResultsController!
    }
    var _fetchedResultsController: NSFetchedResultsController<DigitalCurrencyDepositAddress>? = nil
    
    // MARK: - FRC delegate
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView?.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView?.insertSections(IndexSet(integer: sectionIndex), with: .automatic)
        case .delete:
            tableView?.deleteSections(IndexSet(integer: sectionIndex), with: .automatic)
        default:
            return
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            tableView?.insertRows(at: [newIndexPath!], with: .automatic)
        case .delete:
            tableView?.deleteRows(at: [indexPath!], with: .automatic)
        case .update:
            if let cell = tableView.cellForRow(at: indexPath!) as? DepositAddressTableViewCell {
                cell.address = fetchedResultsController.object(at: indexPath!)
            }
        case .move:
            if let cell = tableView.cellForRow(at: indexPath!) as? DepositAddressTableViewCell {
                cell.address = fetchedResultsController.object(at: indexPath!)
            }
            tableView?.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView?.endUpdates()
    }
}
