//
//  DepositAddressTableViewCell.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 20/5/18.
//  Copyright © 2018 Ruben Zilibowitz. All rights reserved.
//

import UIKit

class DepositAddressTableViewCell: UITableViewCell {

    var address: DigitalCurrencyDepositAddress? {
        didSet {
            configureCell()
        }
    }
    
    @IBOutlet weak var depositAddressLabel: UILabel!
    @IBOutlet weak var lastCheckedTimestampLabel: UILabel!
    @IBOutlet weak var nextCheckedTimestampLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell() {
        guard let address = self.address else {
            depositAddressLabel.text = nil
            lastCheckedTimestampLabel.text = nil
            nextCheckedTimestampLabel.text = nil
            return
        }
        depositAddressLabel.text = address.depositAddress
        lastCheckedTimestampLabel.text = address.lastCheckedTimestamp?.shortString
        nextCheckedTimestampLabel.text = address.nextUpdateTimestamp?.shortString

    }
}
