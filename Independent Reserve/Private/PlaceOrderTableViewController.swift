//
//  PlaceOrderTableViewController.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 22/5/18.
//  Copyright © 2018 Ruben Zilibowitz. All rights reserved.
//

import UIKit
import CoreData

class PlaceOrderTableViewController: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource, NSFetchedResultsControllerDelegate {

    @IBOutlet weak var primaryCurrencyPicker: UIPickerView!
    @IBOutlet weak var secondaryCurrencyPicker: UIPickerView!
    @IBOutlet weak var orderTypePicker: UIPickerView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var volumeTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        downloadLatest()
        updatePrice()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func tappedExecute(_ sender: UIBarButtonItem) {
        guard let orderType = (orderType_fetchedResultsController.sections?[0].objects?[orderTypePicker.selectedRow(inComponent: 0)] as? ValidOrderType), let typeString = orderType.type else { return }
        guard let primaryCurrencyCode = (primaryCurrency_fetchedResultsController.sections?[0].objects?[primaryCurrencyPicker.selectedRow(inComponent: 0)] as? ValidPrimaryCurrencyCode)?.code else { return }
        guard let secondaryCurrencyCode = (secondaryCurrency_fetchedResultsController.sections?[0].objects?[secondaryCurrencyPicker.selectedRow(inComponent: 0)] as? ValidSecondaryCurrencyCode)?.code else { return }
        
        guard let volumeTextFieldText = volumeTextField.text,
            let volume = Double(volumeTextFieldText) else { return }
        
        if orderType.isLimitOrder {
            guard let priceTextFieldText = priceTextField.text,
                let price = Double(priceTextFieldText) else { return }
            
            let alert = UIAlertController(title: "Are you sure you want to place a limit order with the following details?", message: "primary currency: \(primaryCurrencyCode)\nsecondary currency: \(secondaryCurrencyCode)\norder type: \(orderType.type ?? "?")\nprice: \(price)\nvolume: \(volume)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Continue", style: .`default`, handler: {action in
                IRAPIService.shared.PlaceLimitOrder(primaryCurrencyCode: primaryCurrencyCode, secondaryCurrencyCode: secondaryCurrencyCode, orderType: typeString, price: price, volume: volume, completion: {result in
                    switch result {
                    case .success(let orderGuid):
                        DispatchQueue.main.async {[weak self] in
                            self?.performSegue(withIdentifier: "showOrderDetails", sender: orderGuid)
                        }
                    case .failure(let error):
                        DispatchQueue.main.async {[weak self] in self?.presentError(message: error?.localizedDescription ?? "error")}
                    }
                })
            }))
            
            present(alert, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertController(title: "Are you sure you want to place a market order with the following details?", message: "primary currency: \(primaryCurrencyCode)\nsecondary currency: \(secondaryCurrencyCode)\norder type: \(orderType.type ?? "?")\nvolume: \(volume)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Continue", style: .`default`, handler: {action in
                IRAPIService.shared.PlaceMarketOrder(primaryCurrencyCode: primaryCurrencyCode, secondaryCurrencyCode: secondaryCurrencyCode, orderType: typeString, volume: volume, completion: {result in
                    switch result {
                    case .success(let orderGuid):
                        DispatchQueue.main.async {[weak self] in
                            self?.performSegue(withIdentifier: "showOrderDetails", sender: orderGuid)
                        }
                    case .failure(let error):
                        DispatchQueue.main.async {[weak self] in self?.presentError(message: error?.localizedDescription ?? "error")}
                    }
                })
            }))
            
            present(alert, animated: true, completion: nil)
        }
    }
    
    func downloadLatest() {
        IRAPIService.shared.GetValidPrimaryCurrencyCodes(completion: {_ in
            DispatchQueue.main.async {
                IRAPIService.shared.GetValidSecondaryCurrencyCodes(completion: {_ in
                })
            }
        })
        
        IRAPIService.shared.GetValidLimitOrderTypes(completion: {_ in
            DispatchQueue.main.async {
                IRAPIService.shared.GetValidMarketOrderTypes(completion: {_ in
                })
            }
        })
    }
    
    func updatePrice() {
        guard let primaryCurrencyCode = (primaryCurrency_fetchedResultsController.sections?[0].objects?[primaryCurrencyPicker.selectedRow(inComponent: 0)] as? ValidPrimaryCurrencyCode)?.code else { return }
        guard let secondaryCurrencyCode = (secondaryCurrency_fetchedResultsController.sections?[0].objects?[secondaryCurrencyPicker.selectedRow(inComponent: 0)] as? ValidSecondaryCurrencyCode)?.code else { return }
        guard let orderType = (orderType_fetchedResultsController.sections?[0].objects?[orderTypePicker.selectedRow(inComponent: 0)] as? ValidOrderType) else { return }
        IRAPIService.shared.GetMarketSummary(primaryCurrencyCode: primaryCurrencyCode, secondaryCurrencyCode: secondaryCurrencyCode, completion: {result in
            switch result {
            case .success(let marketSummary):
                DispatchQueue.main.async {[weak self] in
                    guard let weakSelf = self else { return }
                    guard weakSelf.priceTextField.isEnabled else { return }
                    if orderType.isBid {
                        weakSelf.priceTextField.text = "\(marketSummary.currentHighestBidPrice)"
                    }
                    else {
                        weakSelf.priceTextField.text = "\(marketSummary.currentLowestOfferPrice)"
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async {[weak self] in self?.presentError(message: error?.localizedDescription ?? "error")}
            }
        })
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showOrderDetails", let destination = segue.destination as? OrderDetailsTableViewController {
            destination.orderGuid = sender as? String
        }
    }
    
    // MARK: - UIPickerViewDataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if pickerView === primaryCurrencyPicker {
            return primaryCurrency_fetchedResultsController.sections?.count ?? 0
        }
        else if pickerView === secondaryCurrencyPicker {
            return secondaryCurrency_fetchedResultsController.sections?.count ?? 0
        }
        else if pickerView === orderTypePicker {
            return orderType_fetchedResultsController.sections?.count ?? 0
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView === primaryCurrencyPicker {
            return primaryCurrency_fetchedResultsController.sections?[component].numberOfObjects ?? 0
        }
        else if pickerView === secondaryCurrencyPicker {
            return secondaryCurrency_fetchedResultsController.sections?[component].numberOfObjects ?? 0
        }
        else if pickerView === orderTypePicker {
            return orderType_fetchedResultsController.sections?[component].numberOfObjects ?? 0
        }
        
        return 0
    }
    
    // MARK: - UIPickerViewDelegate
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView === primaryCurrencyPicker {
            return (primaryCurrency_fetchedResultsController.sections?[component].objects?[row] as? ValidPrimaryCurrencyCode)?.code
        }
        else if pickerView === secondaryCurrencyPicker {
            return (secondaryCurrency_fetchedResultsController.sections?[component].objects?[row] as? ValidSecondaryCurrencyCode)?.code
        }
        else if pickerView === orderTypePicker {
            return (orderType_fetchedResultsController.sections?[component].objects?[row] as? ValidOrderType)?.type
        }
        
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView === primaryCurrencyPicker {
        }
        else if pickerView === secondaryCurrencyPicker {
        }
        else if pickerView === orderTypePicker {
            guard let orderType = (orderType_fetchedResultsController.sections?[component].objects?[row] as? ValidOrderType) else { return }
            if !orderType.isLimitOrder {
                priceTextField.isEnabled = false
                priceLabel.textColor = .lightGray
                priceTextField.text = "Market price"
                priceTextField.textColor = .lightGray
            }
            else {
                priceTextField.isEnabled = true
                priceLabel.textColor = .black
                priceTextField.text = nil
                priceTextField.textColor = .black
            }
        }
        
        updatePrice()
    }
    
    // MARK: - Fetched results controller
    
    var primaryCurrency_fetchedResultsController: NSFetchedResultsController<ValidPrimaryCurrencyCode> {
        if _primaryCurrency_fetchedResultsController != nil {
            return _primaryCurrency_fetchedResultsController!
        }
        
        let fetchRequest: NSFetchRequest<ValidPrimaryCurrencyCode> = ValidPrimaryCurrencyCode.fetchRequest()
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        let sortDescriptor = NSSortDescriptor(key: "code", ascending: true, selector: #selector(NSString.caseInsensitiveCompare))
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let managedObjectContext = IRCoreDataStack.shared.persistentContainer.viewContext
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        aFetchedResultsController.delegate = self
        _primaryCurrency_fetchedResultsController = aFetchedResultsController
        
        do {
            try _primaryCurrency_fetchedResultsController!.performFetch()
        } catch {
            let nserror = error as NSError
            presentError(message: "Unresolved error \(nserror), \(nserror.userInfo)")
        }
        
        return _primaryCurrency_fetchedResultsController!
    }
    var _primaryCurrency_fetchedResultsController: NSFetchedResultsController<ValidPrimaryCurrencyCode>? = nil
    
    var secondaryCurrency_fetchedResultsController: NSFetchedResultsController<ValidSecondaryCurrencyCode> {
        if _secondaryCurrency_fetchedResultsController != nil {
            return _secondaryCurrency_fetchedResultsController!
        }
        
        let fetchRequest: NSFetchRequest<ValidSecondaryCurrencyCode> = ValidSecondaryCurrencyCode.fetchRequest()
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        let sortDescriptor = NSSortDescriptor(key: "code", ascending: true, selector: #selector(NSString.caseInsensitiveCompare))
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let managedObjectContext = IRCoreDataStack.shared.persistentContainer.viewContext
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        aFetchedResultsController.delegate = self
        _secondaryCurrency_fetchedResultsController = aFetchedResultsController
        
        do {
            try _secondaryCurrency_fetchedResultsController!.performFetch()
        } catch {
            let nserror = error as NSError
            presentError(message: "Unresolved error \(nserror), \(nserror.userInfo)")
        }
        
        return _secondaryCurrency_fetchedResultsController!
    }
    var _secondaryCurrency_fetchedResultsController: NSFetchedResultsController<ValidSecondaryCurrencyCode>? = nil
    
    var orderType_fetchedResultsController: NSFetchedResultsController<ValidOrderType> {
        if _orderType_fetchedResultsController != nil {
            return _orderType_fetchedResultsController!
        }
        
        let fetchRequest: NSFetchRequest<ValidOrderType> = ValidOrderType.fetchRequest()
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        let sortDescriptor = NSSortDescriptor(key: "type", ascending: true, selector: #selector(NSString.caseInsensitiveCompare))
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let managedObjectContext = IRCoreDataStack.shared.persistentContainer.viewContext
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        aFetchedResultsController.delegate = self
        _orderType_fetchedResultsController = aFetchedResultsController
        
        do {
            try _orderType_fetchedResultsController!.performFetch()
        } catch {
            let nserror = error as NSError
            presentError(message: "Unresolved error \(nserror), \(nserror.userInfo)")
        }
        
        return _orderType_fetchedResultsController!
    }
    var _orderType_fetchedResultsController: NSFetchedResultsController<ValidOrderType>? = nil
    
    // MARK: - FRC delegate
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        if controller === primaryCurrency_fetchedResultsController {
            primaryCurrencyPicker.reloadAllComponents()
        }
        else if controller === secondaryCurrency_fetchedResultsController {
            secondaryCurrencyPicker.reloadAllComponents()
        }
        else if controller === orderType_fetchedResultsController {
            orderTypePicker.reloadAllComponents()
        }
    }
}
