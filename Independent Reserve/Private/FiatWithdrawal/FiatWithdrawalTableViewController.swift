//
//  FiatWithdrawalTableViewController.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 24/5/18.
//  Copyright © 2018 Ruben Zilibowitz. All rights reserved.
//

import UIKit

class FiatWithdrawalTableViewController: UITableViewController {

    var fiatWithdrawal: FiatWithdrawal?
    
    @IBOutlet weak var requestGuidLabel: UILabel!
    @IBOutlet weak var accountGuidLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var createdTimestampLabel: UILabel!
    @IBOutlet weak var withdrawalAmountLabel: UILabel!
    @IBOutlet weak var feeAmountLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateTable()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func updateTable() {
        guard let fiatWithdrawal = self.fiatWithdrawal else { return }
        
        requestGuidLabel.text = fiatWithdrawal.fiatWithdrawalRequestGuid
        accountGuidLabel.text = fiatWithdrawal.accountGuid
        statusLabel.text = fiatWithdrawal.status
        createdTimestampLabel.text = fiatWithdrawal.createdTimestamp?.shortString
        withdrawalAmountLabel.text = "\(fiatWithdrawal.totalWithdrawalAmount)"
        feeAmountLabel.text = "\(fiatWithdrawal.feeAmount)"
        currencyLabel.text = fiatWithdrawal.relatedSecondaryCurrency?.code
    }
}
