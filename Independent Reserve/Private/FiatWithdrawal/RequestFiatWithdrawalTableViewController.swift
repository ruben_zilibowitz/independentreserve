//
//  RequestFiatWithdrawalTableViewController.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 24/5/18.
//  Copyright © 2018 Ruben Zilibowitz. All rights reserved.
//

import UIKit
import CoreData

class RequestFiatWithdrawalTableViewController: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource, NSFetchedResultsControllerDelegate {

    @IBOutlet weak var currencyPicker: UIPickerView!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var bankAccountNameTextField: UITextField!
    @IBOutlet weak var commentTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func tappedExecute(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Are you sure?", message: "This action cannot be undone.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Continue", style: .`default`, handler: {[weak self] action in
            guard let weakSelf = self else { return }
            guard let amount = weakSelf.amountTextField.text else { return }
            guard let comment = weakSelf.commentTextField.text else { return }
            guard let bankAccountName = weakSelf.bankAccountNameTextField.text else { return }
            guard let secondaryCurrencyCode = weakSelf.secondaryCurrency_fetchedResultsController.object(at: IndexPath(row: weakSelf.currencyPicker.selectedRow(inComponent: 0), section: 0)).code else { return }
            IRAPIService.shared.RequestFiatWithdrawal(secondaryCurrencyCode: secondaryCurrencyCode, withdrawalAmount: amount, withdrawalBankAccountName: bankAccountName, comment: comment, completion: {fiatWithdrawal in
                
                weakSelf.performSegue(withIdentifier: "showFiatWithdrawal", sender: fiatWithdrawal)
            })
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return secondaryCurrency_fetchedResultsController.sections?.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return secondaryCurrency_fetchedResultsController.sections?[component].numberOfObjects ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return secondaryCurrency_fetchedResultsController.object(at: IndexPath(row: row, section: component)).code
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showFiatWithdrawal",
            let fiatWithdrawal = sender as? FiatWithdrawal,
            let destination = segue.destination as? FiatWithdrawalTableViewController {
            destination.fiatWithdrawal = fiatWithdrawal
        }
    }
    
    // MARK: - Fetched results controller
    
    var secondaryCurrency_fetchedResultsController: NSFetchedResultsController<ValidSecondaryCurrencyCode> {
        if _secondaryCurrency_fetchedResultsController != nil {
            return _secondaryCurrency_fetchedResultsController!
        }
        
        let fetchRequest: NSFetchRequest<ValidSecondaryCurrencyCode> = ValidSecondaryCurrencyCode.fetchRequest()
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        let sortDescriptor = NSSortDescriptor(key: "code", ascending: true, selector: #selector(NSString.caseInsensitiveCompare))
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let managedObjectContext = IRCoreDataStack.shared.persistentContainer.viewContext
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        aFetchedResultsController.delegate = self
        _secondaryCurrency_fetchedResultsController = aFetchedResultsController
        
        do {
            try _secondaryCurrency_fetchedResultsController!.performFetch()
        } catch {
            let nserror = error as NSError
            presentError(message: "Unresolved error \(nserror), \(nserror.userInfo)")
        }
        
        return _secondaryCurrency_fetchedResultsController!
    }
    var _secondaryCurrency_fetchedResultsController: NSFetchedResultsController<ValidSecondaryCurrencyCode>? = nil

    // MARK: - FRC delegate
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        currencyPicker.reloadAllComponents()
    }
}
