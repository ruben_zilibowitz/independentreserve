//
//  PrivateOrderTableViewCell.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 22/5/18.
//  Copyright © 2018 Ruben Zilibowitz. All rights reserved.
//

import UIKit

class PrivateOrderTableViewCell: UITableViewCell {

    var privateOrder: PrivateOrder? {
        didSet {
            configureCell()
        }
    }
    
    @IBOutlet weak var averagePriceLabel: UILabel!
    @IBOutlet weak var createdTimestampLabel: UILabel!
    @IBOutlet weak var feePercentLabel: UILabel!
    @IBOutlet weak var orderGuidLabel: UILabel!
    @IBOutlet weak var orderTypeLabel: UILabel!
    @IBOutlet weak var outstandingLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var primaryCurrencyLabel: UILabel!
    @IBOutlet weak var secondaryCurrencyLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var volumeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell() {
        guard let privateOrder = self.privateOrder else {
            averagePriceLabel.text = nil
            createdTimestampLabel.text = nil
            feePercentLabel.text = nil
            orderGuidLabel.text = nil
            orderTypeLabel.text = nil
            outstandingLabel.text = nil
            priceLabel.text = nil
            primaryCurrencyLabel.text = nil
            secondaryCurrencyLabel.text = nil
            statusLabel.text = nil
            valueLabel.text = nil
            volumeLabel.text = nil
            return
        }
        
        averagePriceLabel.text = "\(privateOrder.averagePrice)"
        createdTimestampLabel.text = privateOrder.createdTimestamp?.shortString
        feePercentLabel.text = "\(privateOrder.feePercent)"
        orderGuidLabel.text = privateOrder.orderGuid
        orderTypeLabel.text = privateOrder.relatedOrderType?.type
        outstandingLabel.text = "\(privateOrder.outstanding)"
        priceLabel.text = "\(privateOrder.price)"
        primaryCurrencyLabel.text = privateOrder.relatedPrimaryCurrency?.code
        secondaryCurrencyLabel.text = privateOrder.relatedSecondaryCurrency?.code
        statusLabel.text = privateOrder.status
        valueLabel.text = "\(privateOrder.value)"
        volumeLabel.text = "\(privateOrder.volumeOrdered)"
    }
}
