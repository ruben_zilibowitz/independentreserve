//
//  OrdersTableViewController.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 21/5/18.
//  Copyright © 2018 Ruben Zilibowitz. All rights reserved.
//

import UIKit
import CoreData

class OrdersTableViewController: UITableViewController, NSFetchedResultsControllerDelegate, UsesCurrency {
    
    var primaryCurrency: ValidPrimaryCurrencyCode?
    var secondaryCurrency: ValidSecondaryCurrencyCode?
    
    enum OrderType: Int {
        case open = 0
        case closed = 1
        case closedFilled = 2
    }
    
    var orderType: OrderType = .open
    
    @IBInspectable var orderTypeFromInt: Int = OrderType.open.rawValue {
        didSet {
            orderType = OrderType(rawValue: orderTypeFromInt) ?? .open
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        update()
    }
    
    func update() {
        switch orderType {
        case .open:
            IRAPIService.shared.GetOpenOrders(primaryCurrencyCode: primaryCurrency?.code, secondaryCurrencyCode: secondaryCurrency?.code, completion: {result in
                switch result {
                case .failure(let error):
                    DispatchQueue.main.async {[weak self] in self?.presentError(message: error?.localizedDescription ?? "error")}
                case .success(let count):
                    NSLog("%d", count)
                }
            })
        case .closed:
            IRAPIService.shared.GetClosedOrders(primaryCurrencyCode: primaryCurrency?.code, secondaryCurrencyCode: secondaryCurrency?.code, completion: {result in
                switch result {
                case .failure(let error):
                    DispatchQueue.main.async {[weak self] in self?.presentError(message: error?.localizedDescription ?? "error")}
                case .success(let count):
                    NSLog("%d", count)
                }
            })
        case .closedFilled:
            IRAPIService.shared.GetClosedFilledOrders(primaryCurrencyCode: primaryCurrency?.code, secondaryCurrencyCode: secondaryCurrency?.code, completion: {result in
                switch result {
                case .failure(let error):
                    DispatchQueue.main.async {[weak self] in self?.presentError(message: error?.localizedDescription ?? "error")}
                case .success(let count):
                    NSLog("%d", count)
                }
            })
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showOrderDetails",
            let destination = segue.destination as? OrderDetailsTableViewController,
            let cell = sender as? PrivateOrderTableViewCell {
            destination.orderGuid = cell.privateOrder?.orderGuid
        }
    }
    
    @IBAction func tappedRefresh(_ sender: UIBarButtonItem) {
        update()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PrivateOrderTableViewCell", for: indexPath) as? PrivateOrderTableViewCell else { return UITableViewCell() }
        
        cell.privateOrder = fetchedResultsController.object(at: indexPath)
        
        return cell
    }
    
    // MARK: - Fetched results controller
    
    var fetchedResultsController: NSFetchedResultsController<PrivateOrder> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest: NSFetchRequest<PrivateOrder> = PrivateOrder.fetchRequest()
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        let sortDescriptor = NSSortDescriptor(key: "createdTimestamp", ascending: false, selector: nil)
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        var predicate = orderType == .open ? NSPredicate(format: "status = %@", "Open") : NSPredicate(format: "status != %@", "Open")
        if let primaryCurrencyCode = primaryCurrency?.code {
            predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate, NSPredicate(format: "relatedPrimaryCurrency.code = %@", primaryCurrencyCode)])
        }
        if let secondaryCurrencyCode = secondaryCurrency?.code {
            predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate, NSPredicate(format: "relatedSecondaryCurrency.code = %@", secondaryCurrencyCode)])
        }
        
        fetchRequest.predicate = predicate
        
        let managedObjectContext = IRCoreDataStack.shared.persistentContainer.viewContext
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            let nserror = error as NSError
            presentError(message: "Unresolved error \(nserror), \(nserror.userInfo)")
        }
        
        return _fetchedResultsController!
    }
    var _fetchedResultsController: NSFetchedResultsController<PrivateOrder>? = nil
    
    // MARK: - FRC delegate
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView?.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView?.insertSections(IndexSet(integer: sectionIndex), with: .automatic)
        case .delete:
            tableView?.deleteSections(IndexSet(integer: sectionIndex), with: .automatic)
        default:
            return
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            tableView?.insertRows(at: [newIndexPath!], with: .automatic)
        case .delete:
            tableView?.deleteRows(at: [indexPath!], with: .automatic)
        case .update:
            if let cell = tableView.cellForRow(at: indexPath!) as? PrivateOrderTableViewCell {
                cell.privateOrder = fetchedResultsController.object(at: indexPath!)
            }
        case .move:
            if let cell = tableView.cellForRow(at: indexPath!) as? PrivateOrderTableViewCell {
                cell.privateOrder = fetchedResultsController.object(at: indexPath!)
            }
            tableView?.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView?.endUpdates()
    }
}

