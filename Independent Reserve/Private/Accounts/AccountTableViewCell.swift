//
//  AccountTableViewCell.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 19/5/18.
//  Copyright © 2018 Ruben Zilibowitz. All rights reserved.
//

import UIKit

class AccountTableViewCell: UITableViewCell {

    var account: Account? {
        didSet {
            configureCell()
        }
    }
    
    @IBOutlet weak var guidLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var availableBalanceLabel: UILabel!
    @IBOutlet weak var totalBalanceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell() {
        guard let account = self.account else {
            guidLabel.text = nil
            currencyLabel.text = nil
            statusLabel.text = nil
            availableBalanceLabel.text = nil
            totalBalanceLabel.text = nil
            return
        }
        
        guidLabel.text = account.accountGuid
        currencyLabel.text = account.relatedCurrency?.code
        statusLabel.text = account.accountStatus
        availableBalanceLabel.text = "\(account.availableBalance)"
        totalBalanceLabel.text = "\(account.totalBalance)"
    }
}
