//
//  TransactionTableViewCell.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 20/5/18.
//  Copyright © 2018 Ruben Zilibowitz. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {

    var transaction: Transaction? {
        didSet {
            configureCell()
        }
    }
    
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var createdTimestampLabel: UILabel!
    @IBOutlet weak var creditLabel: UILabel!
    @IBOutlet weak var debitLabel: UILabel!
    @IBOutlet weak var settleTimestampLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell() {
        guard let transaction = self.transaction else {
            balanceLabel.text = nil
            commentLabel.text = nil
            createdTimestampLabel.text = nil
            creditLabel.text = nil
            debitLabel.text = nil
            settleTimestampLabel.text = nil
            statusLabel.text = nil
            typeLabel.text = nil
            return
        }
        
        balanceLabel.text = "\(transaction.balance)"
        commentLabel.text = transaction.comment
        createdTimestampLabel.text = transaction.createdTimestamp?.shortString
        creditLabel.text = "\(transaction.credit)"
        debitLabel.text = "\(transaction.debit)"
        settleTimestampLabel.text = transaction.settleTimestamp?.shortString
        statusLabel.text = transaction.status
        typeLabel.text = transaction.relatedTransactionType?.type
    }
}
