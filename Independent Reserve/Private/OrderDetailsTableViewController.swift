//
//  OrderDetailsTableViewController.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 20/5/18.
//  Copyright © 2018 Ruben Zilibowitz. All rights reserved.
//

import UIKit
import CoreData

class OrderDetailsTableViewController: UITableViewController, NSFetchedResultsControllerDelegate {

    var orderGuid: String?
    
    @IBOutlet weak var orderGuidLabel: UILabel!
    @IBOutlet weak var createdTimestampLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var volumeOrderedLabel: UILabel!
    @IBOutlet weak var volumeFilledLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var averagePriceLabel: UILabel!
    @IBOutlet weak var reservedAmountLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var primaryCurrencyCodeLabel: UILabel!
    @IBOutlet weak var secondaryCurrencyCodeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        configureTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let orderGuid = self.orderGuid else { return }
        update(orderGuid: orderGuid)
        
        _ = fetchedResultsController
    }
    
    @IBAction func tappedRefresh(_ sender: UIBarButtonItem) {
        guard let orderGuid = self.orderGuid else { return }
        update(orderGuid: orderGuid)
    }
    
    func update(orderGuid: String) {
        IRAPIService.shared.GetOrderDetails(orderGuid: orderGuid, completion: {result in
            switch result {
            case .failure(let error):
                DispatchQueue.main.async {[weak self] in self?.presentError(message: error?.localizedDescription ?? "error")}
            case .success(let message):
                NSLog(message)
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureTable() {
        guard let order = fetchedResultsController.fetchedObjects?.first else { return }
        
        orderGuidLabel.text = order.orderGuid
        createdTimestampLabel.text = order.createdTimestamp?.shortString
        typeLabel.text = order.relatedOrderType?.type
        volumeOrderedLabel.text = "\(order.volumeOrdered)"
        volumeFilledLabel.text = "\(order.volumeFilled)"
        priceLabel.text = "\(order.price)"
        averagePriceLabel.text = "\(order.averagePrice)"
        reservedAmountLabel.text = "\(order.reservedAmount)"
        statusLabel.text = order.status
        primaryCurrencyCodeLabel.text = order.relatedPrimaryCurrency?.code
        secondaryCurrencyCodeLabel.text = order.relatedSecondaryCurrency?.code
    }
    
    @IBAction func tappedCancel(_ sender: UIBarButtonItem) {
        guard let orderGuid = self.orderGuid else { return }
        let alert = UIAlertController(title: "Are you sure you want to cancel this order?", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: .`default`, handler: {action in
            IRAPIService.shared.CancelOrder(orderGuid: orderGuid, completion: {[weak self] result in
                switch result {
                case .success(let returnedGuid):
                    self?.update(orderGuid: returnedGuid)
                case .failure(let error):
                    DispatchQueue.main.async {[weak self] in self?.presentError(message: error?.localizedDescription ?? "error")}
                }
            })
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Fetched results controller
    
    var fetchedResultsController: NSFetchedResultsController<PrivateOrder> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest: NSFetchRequest<PrivateOrder> = PrivateOrder.fetchRequest()
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        let sortDescriptor = NSSortDescriptor(key: "createdTimestamp", ascending: false, selector: nil)
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        fetchRequest.predicate = NSPredicate(format: "orderGuid = %@", orderGuid ?? "")
        
        let managedObjectContext = IRCoreDataStack.shared.persistentContainer.viewContext
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            let nserror = error as NSError
            presentError(message: "Unresolved error \(nserror), \(nserror.userInfo)")
        }
        
        return _fetchedResultsController!
    }
    var _fetchedResultsController: NSFetchedResultsController<PrivateOrder>? = nil
    
    // MARK: - FRC delegate
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        configureTable()
    }
}
