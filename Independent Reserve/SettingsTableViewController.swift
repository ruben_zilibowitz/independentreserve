//
//  SettingsTableViewController.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 19/5/18.
//  Copyright © 2018 Ruben Zilibowitz. All rights reserved.
//

import UIKit

let IRAPIKey = "IRAPIKey"
let IRAPISecret = "IRAPISecret"

class SettingsTableViewController: UITableViewController, UITextFieldDelegate {

    @IBOutlet weak var apiKeyTextField: UITextField!
    @IBOutlet weak var apiSecretTextField: UITextField!
    
    @IBAction func tappedSave(_ sender: UIBarButtonItem) {
        if let text = apiKeyTextField.text {
            IRAPIService.shared.apiKey = text
            UserDefaults.standard.set(text, forKey: IRAPIKey)
        }
        
        if let text = apiSecretTextField.text {
            IRAPIService.shared.apiSecret = text
            UserDefaults.standard.set(text, forKey: IRAPISecret)
        }
        
        UserDefaults.standard.synchronize()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        
        apiKeyTextField.delegate = self
        apiSecretTextField.delegate = self
        
        apiKeyTextField.text = IRAPIService.shared.apiKey
        apiSecretTextField.text = IRAPIService.shared.apiSecret
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField === apiKeyTextField {
            apiSecretTextField.becomeFirstResponder()
            return false
        }
        else if textField === apiSecretTextField {
            apiKeyTextField.becomeFirstResponder()
            return false
        }
        else {
            return true
        }
    }
}
