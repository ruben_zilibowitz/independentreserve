//
//  OrderBookTableViewController.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 28/12/17.
//  Copyright © 2017 Ruben Zilibowitz. All rights reserved.
//

import UIKit

class OrderBookTableViewController: UITableViewController, UsesCurrency {

    weak var primaryCurrency: ValidPrimaryCurrencyCode?
    weak var secondaryCurrency: ValidSecondaryCurrencyCode?
    
    var orderBook: OrderBook? {
        didSet {
            tableView.reloadData()
            
            let dateFormatter = DateFormatter()
            dateFormatter.timeStyle = .short
            dateFormatter.dateStyle = .short
            navigationItem.title = orderBook?.createdTimestamp.flatMap({dateFormatter.string(from: $0)})
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        
        if let dateString = orderBook?.createdTimestamp?.shortString {
            navigationItem.title = dateString + " Order Book"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let primaryCurrencyCode = self.primaryCurrency?.code else { return }
        guard let secondaryCurrencyCode = self.secondaryCurrency?.code else { return }
        IRAPIService.shared.GetOrderBook(primaryCurrencyCode: primaryCurrencyCode, secondaryCurrencyCode: secondaryCurrencyCode, completion: {result in
            switch result {
            case .success(let orderBook):
                DispatchQueue.main.async {[weak self] in
                    self?.orderBook = orderBook
                }
            case .failure(let error):
                print(error?.localizedDescription ?? "error")
            }
        })
        
        /*(primaryCurrencyCode: primaryCurrencyCode, secondaryCurrencyCode: secondaryCurrencyCode, completion: {[weak self] result in
            
            switch result {
            case .success(let objectID):
                DispatchQueue.main.async {
                    self?.marketSummary = IRCoreDataStack.shared.persistentContainer.viewContext.object(with: objectID) as? MarketSummary
                }
            case .failure(let error):
                print(error?.localizedDescription ?? "error")
            }
        })*/
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return orderBook?.relatedSellOrders?.count ?? 0
        }
        return orderBook?.relatedBuyOrders?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Sell Orders"
        }
        else {
            return "Buy Orders"
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath)

        guard let order = (indexPath.section == 0 ? orderBook?.relatedSellOrders?[indexPath.row] : orderBook?.relatedBuyOrders?[indexPath.row]) as? PublicOrder else { return UITableViewCell() }
        
        cell.textLabel?.text = "Price: \(order.price)"
        cell.detailTextLabel?.text = "Volume: \(order.volume)"
        
        return cell
    }

    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return ["Sell", "Buy"]
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
