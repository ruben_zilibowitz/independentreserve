//
//  MarketSummaryTableViewController.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 12/12/17.
//  Copyright © 2017 Ruben Zilibowitz. All rights reserved.
//

import UIKit

class MarketSummaryTableViewController: UITableViewController, UsesCurrency {

    weak var primaryCurrency: ValidPrimaryCurrencyCode?
    weak var secondaryCurrency: ValidSecondaryCurrencyCode?
    
    var marketSummary: MarketSummary? {
        didSet {
            configureCells()
        }
    }
    
    @IBOutlet weak var currencyCell: UITableViewCell!
    @IBOutlet weak var timestampCell: UITableViewCell!
    @IBOutlet weak var highestBidPriceCell: UITableViewCell!
    @IBOutlet weak var lowestOfferPriceCell: UITableViewCell!
    @IBOutlet weak var dayAveragePriceCell: UITableViewCell!
    @IBOutlet weak var dayHighestPriceCell: UITableViewCell!
    @IBOutlet weak var dayLowestPriceCell: UITableViewCell!
    @IBOutlet weak var dayVolumeXbtCell: UITableViewCell!
    @IBOutlet weak var dayVolumeXbtInSecondaryCurrencyCell: UITableViewCell!
    @IBOutlet weak var lastPriceCell: UITableViewCell!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let primaryCurrencyCode = self.primaryCurrency?.code else { return }
        guard let secondaryCurrencyCode = self.secondaryCurrency?.code else { return }
        IRAPIService.shared.GetMarketSummary(primaryCurrencyCode: primaryCurrencyCode, secondaryCurrencyCode: secondaryCurrencyCode, completion: {result in
            
            switch result {
            case .success(let marketSummary):
                DispatchQueue.main.async {[weak self] in self?.marketSummary = marketSummary }
            case .failure(let error):
                print(error?.localizedDescription ?? "error")
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureCells() {
        guard let marketSummary = self.marketSummary else { return }
        guard let primaryCode = marketSummary.relatedPrimaryCurrency?.code else { return }
        guard let secondaryCode = marketSummary.relatedSecondaryCurrency?.code else { return }
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .short
        dateFormatter.dateStyle = .short
        
        currencyCell.detailTextLabel?.text = "\(primaryCode) / \(secondaryCode)"
        timestampCell.detailTextLabel?.text = marketSummary.createdTimestamp.flatMap({dateFormatter.string(from: $0)})
        highestBidPriceCell.detailTextLabel?.text = marketSummary.currentHighestBidPrice.description
        lowestOfferPriceCell.detailTextLabel?.text = marketSummary.currentLowestOfferPrice.description
        dayAveragePriceCell.detailTextLabel?.text = marketSummary.dayAveragePrice.description
        dayHighestPriceCell.detailTextLabel?.text = marketSummary.dayHighestPrice.description
        dayLowestPriceCell.detailTextLabel?.text = marketSummary.dayLowestPrice.description
        dayVolumeXbtCell.detailTextLabel?.text = marketSummary.dayVolumeXbt.description
        dayVolumeXbtInSecondaryCurrencyCell.detailTextLabel?.text = marketSummary.dayVolumeXbtInSecondaryCurrency.description
        lastPriceCell.detailTextLabel?.text = marketSummary.lastPrice.description
    }
}
