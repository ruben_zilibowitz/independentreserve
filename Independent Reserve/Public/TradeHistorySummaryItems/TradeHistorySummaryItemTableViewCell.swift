//
//  TradeHistorySummaryItemTableViewCell.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 22/5/18.
//  Copyright © 2018 Ruben Zilibowitz. All rights reserved.
//

import UIKit

class TradeHistorySummaryItemTableViewCell: UITableViewCell {

    var summaryItem: TradeHistorySummaryItem? {
        didSet {
            configureCell()
        }
    }
    
    @IBOutlet weak var averageSecondaryCurrencyPriceLabel: UILabel!
    @IBOutlet weak var closingSecondaryCurrencyPriceLabel: UILabel!
    @IBOutlet weak var startTimestampLabel: UILabel!
    @IBOutlet weak var endTimestampLabel: UILabel!
    @IBOutlet weak var highestSecondaryCurrencyPriceLabel: UILabel!
    @IBOutlet weak var lowestSecondaryCurrencyPriceLabel: UILabel!
    @IBOutlet weak var numberOfTradesLabel: UILabel!
    @IBOutlet weak var openingSecondaryCurrencyPriceLabel: UILabel!
    @IBOutlet weak var primaryCurrencyVolumeLabel: UILabel!
    @IBOutlet weak var secondaryCurrencyVolumeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell() {
        guard let summaryItem = self.summaryItem else {
            averageSecondaryCurrencyPriceLabel.text = nil
            closingSecondaryCurrencyPriceLabel.text = nil
            startTimestampLabel.text = nil
            endTimestampLabel.text = nil
            highestSecondaryCurrencyPriceLabel.text = nil
            lowestSecondaryCurrencyPriceLabel.text = nil
            numberOfTradesLabel.text = nil
            openingSecondaryCurrencyPriceLabel.text = nil
            primaryCurrencyVolumeLabel.text = nil
            secondaryCurrencyVolumeLabel.text = nil
            return
        }
        
        averageSecondaryCurrencyPriceLabel.text = "\(summaryItem.averageSecondaryCurrencyPrice)"
        closingSecondaryCurrencyPriceLabel.text = "\(summaryItem.closingSecondaryCurrencyPrice)"
        startTimestampLabel.text = summaryItem.startTimestamp?.shortString
        endTimestampLabel.text = summaryItem.endTimestamp?.shortString
        highestSecondaryCurrencyPriceLabel.text = "\(summaryItem.highestSecondaryCurrencyPrice)"
        lowestSecondaryCurrencyPriceLabel.text = "\(summaryItem.lowestSecondaryCurrencyPrice)"
        numberOfTradesLabel.text = "\(summaryItem.numberOfTrades)"
        openingSecondaryCurrencyPriceLabel.text = "\(summaryItem.openingSecondaryCurrencyPrice)"
        primaryCurrencyVolumeLabel.text = "\(summaryItem.primaryCurrencyVolume)"
        secondaryCurrencyVolumeLabel.text = "\(summaryItem.secondaryCurrencyVolume)"
    }
}
