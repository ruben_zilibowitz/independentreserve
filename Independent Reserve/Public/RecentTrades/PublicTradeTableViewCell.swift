//
//  PublicTradeTableViewCell.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 22/5/18.
//  Copyright © 2018 Ruben Zilibowitz. All rights reserved.
//

import UIKit

class PublicTradeTableViewCell: UITableViewCell {

    var trade: PublicTrade? {
        didSet {
            configureCell()
        }
    }
    
    @IBOutlet weak var primaryCurrencyAmountLabel: UILabel!
    @IBOutlet weak var secondaryCurrencyTradePriceLabel: UILabel!
    @IBOutlet weak var tradeTimestampLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell() {
        guard let trade = self.trade else {
            primaryCurrencyAmountLabel.text = nil
            secondaryCurrencyTradePriceLabel.text = nil
            tradeTimestampLabel.text = nil
            return
        }
        
        primaryCurrencyAmountLabel.text = "\(trade.primaryCurrencyAmount)"
        secondaryCurrencyTradePriceLabel.text = "\(trade.secondaryCurrencyTradePrice)"
        tradeTimestampLabel.text = trade.tradeTimestamp?.shortString
    }
}
