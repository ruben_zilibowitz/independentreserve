//
//  PublicTradesTableViewController.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 22/5/18.
//  Copyright © 2018 Ruben Zilibowitz. All rights reserved.
//

import UIKit
import CoreData

class PublicTradesTableViewController: UITableViewController, NSFetchedResultsControllerDelegate, UsesCurrency {
    
    var primaryCurrency: ValidPrimaryCurrencyCode?
    var secondaryCurrency: ValidSecondaryCurrencyCode?

    var recentTrades: RecentTrades?
    
    @IBAction func tappedRefresh(_ sender: UIBarButtonItem) {
        update()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        update()
    }
    
    func update() {
        guard let primaryCurrencyCode = primaryCurrency?.code,
            let secondaryCurrencyCode = secondaryCurrency?.code else { return }
        
        IRAPIService.shared.GetRecentTrades(primaryCurrencyCode: primaryCurrencyCode, secondaryCurrencyCode: secondaryCurrencyCode, completion: {result in
            switch result {
            case .failure(let error):
                DispatchQueue.main.async {[weak self] in self?.presentError(message: error?.localizedDescription ?? "error")}
            case .success(let recentTrades):
                self.recentTrades = recentTrades
                DispatchQueue.main.async {[weak self] in
                    guard let weakSelf = self else { return }
                    weakSelf._fetchedResultsController = nil
                    weakSelf.tableView.reloadData()
                }
            }
        })
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PublicTradeTableViewCell", for: indexPath) as? PublicTradeTableViewCell else { return UITableViewCell() }
        
        cell.trade = fetchedResultsController.object(at: indexPath)
        
        return cell
    }
    
    // MARK: - Fetched results controller
    
    var fetchedResultsController: NSFetchedResultsController<PublicTrade> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest: NSFetchRequest<PublicTrade> = PublicTrade.fetchRequest()
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        let sortDescriptor = NSSortDescriptor(key: "tradeTimestamp", ascending: false, selector: nil)
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        if let recentTrades = self.recentTrades {
            fetchRequest.predicate = NSPredicate(format: "relatedRecentTrades = %@", recentTrades)
        }
        
        let managedObjectContext = IRCoreDataStack.shared.persistentContainer.viewContext
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            let nserror = error as NSError
            presentError(message: "Unresolved error \(nserror), \(nserror.userInfo)")
        }
        
        return _fetchedResultsController!
    }
    var _fetchedResultsController: NSFetchedResultsController<PublicTrade>? = nil
    
    // MARK: - FRC delegate
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView?.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView?.insertSections(IndexSet(integer: sectionIndex), with: .automatic)
        case .delete:
            tableView?.deleteSections(IndexSet(integer: sectionIndex), with: .automatic)
        default:
            return
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            tableView?.insertRows(at: [newIndexPath!], with: .automatic)
        case .delete:
            tableView?.deleteRows(at: [indexPath!], with: .automatic)
        case .update:
            if let cell = tableView.cellForRow(at: indexPath!) as? PublicTradeTableViewCell {
                cell.trade = fetchedResultsController.object(at: indexPath!)
            }
        case .move:
            if let cell = tableView.cellForRow(at: indexPath!) as? PublicTradeTableViewCell {
                cell.trade = fetchedResultsController.object(at: indexPath!)
            }
            tableView?.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView?.endUpdates()
    }
}
