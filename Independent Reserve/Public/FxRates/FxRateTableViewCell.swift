//
//  FxRateTableViewCell.swift
//  Independent Reserve
//
//  Created by Ruben Zilibowitz on 22/5/18.
//  Copyright © 2018 Ruben Zilibowitz. All rights reserved.
//

import UIKit

class FxRateTableViewCell: UITableViewCell {

    var fxRate: FxRate? {
        didSet {
            configureCell()
        }
    }
    
    @IBOutlet weak var currencyALabel: UILabel!
    @IBOutlet weak var currencyBLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell() {
        guard let fxRate = self.fxRate else {
            currencyALabel.text = nil
            currencyBLabel.text = nil
            rateLabel.text = nil
            return
        }
        currencyALabel.text = fxRate.relatedCurrencyA?.code
        currencyBLabel.text = fxRate.relatedCurrencyB?.code
        rateLabel.text = "\(fxRate.rate)"
    }
}
